﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bike.ConsoleHost
{
    using Bike.Core;

    class Program
    {
        static void Main(string[] args)
        {
            var bikeTask = BikeApplication.Run(null);
            bikeTask.Wait();
        }
    }
}
