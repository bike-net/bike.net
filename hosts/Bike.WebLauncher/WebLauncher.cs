﻿using Bike.WebLauncher;

using Microsoft.Owin;

[assembly: OwinStartup(typeof(WebLauncher))]

namespace Bike.WebLauncher
{
    using Core;

    using Owin;

    public class WebLauncher
    {
        public async void Configuration(IAppBuilder appBuilder)
        {
           await BikeApplication.Run(registrator => registrator.RegisterInstance<IAppBuilder>(appBuilder));
        }
    }
}