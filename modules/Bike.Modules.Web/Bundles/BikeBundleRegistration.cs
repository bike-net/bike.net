﻿namespace Bike.Modules.Web.Bundles
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Optimization;

    using Bike.Common.Interfaces;
    using Bike.Contracts.Web;
    using Bike.Modules.Web.Helpers;
    using Bike.Modules.Web.Providers.Interfaces;
    using Bike.Modules.Web.Utils;

    public class BikeBundleRegistration : IBundleRegistration
    {
        private readonly IList<Tuple<string, string, string>> _cssDirectoriesPaths = new List<Tuple<string, string, string>>();

        private readonly IList<Tuple<string, string>> _cssPaths = new List<Tuple<string, string>>();

        private readonly IList<Tuple<string, string, string>> _scriptsDirectoriesPaths = new List<Tuple<string, string, string>>();

        private readonly IList<Tuple<string, string>> _scriptsPaths = new List<Tuple<string, string>>();

        public Lazy<IWebRecipeProvider> ConfigProvider { get; set; }

        public IEnumerable<IModuleMetadata> ModuleMetadatas { get; set; }

        public void AddCss(string moduleName, string relativePath)
        {
            _cssPaths.Add(Tuple.Create(moduleName, relativePath));
        }

        public void AddCssDirectory(string moduleName, string relativePath, string searchPattern = "*.css")
        {
            _cssDirectoriesPaths.Add(Tuple.Create(moduleName, relativePath, searchPattern));
        }

        public void AddScript(string moduleName, string relativePath)
        {
            _scriptsPaths.Add(Tuple.Create(moduleName, relativePath));
        }

        public void AddScriptDirectory(string moduleName, string relativePath, string searchPattern = "*.js")
        {
            _scriptsDirectoriesPaths.Add(Tuple.Create(moduleName, relativePath, searchPattern));
        }

        public void Register()
        {
            RegisterScripts();

            RegisterCss();
        }

        private void RegisterCss()
        {
            var cssBundle = new StyleBundle(ConfigProvider.Value.CssBundlePath);

            foreach (var cssPath in _cssPaths)
            {
                var metadata = ModuleMetadatas.First(mm => mm.FullName == cssPath.Item1);

                var fullPath = VirtualPath.Concat(metadata.GetModuleVirtualPath(ConfigProvider.Value), cssPath.Item2);

                cssBundle.Include(fullPath);
            }

            foreach (var cssDirectoriesPaths in _cssDirectoriesPaths)
            {
                var metadata = ModuleMetadatas.First(mm => mm.FullName == cssDirectoriesPaths.Item1);

                var fullPath = VirtualPath.Concat(metadata.GetModuleVirtualPath(ConfigProvider.Value), cssDirectoriesPaths.Item2);

                cssBundle.IncludeDirectory(fullPath, cssDirectoriesPaths.Item3, true);
            }

            BundleTable.Bundles.Add(cssBundle);
        }

        private void RegisterScripts()
        {
            var scriptBundle = new CustomScriptBundle(ConfigProvider.Value.JsBundlePath);

            foreach (var scriptsPath in _scriptsPaths)
            {
                var metadata = ModuleMetadatas.First(mm => mm.FullName == scriptsPath.Item1);

                var fullPath = VirtualPath.Concat(metadata.GetModuleVirtualPath(ConfigProvider.Value), scriptsPath.Item2);

                scriptBundle.Include(fullPath);
            }

            foreach (var scriptDirectoryPath in _scriptsDirectoriesPaths)
            {
                var metadata = ModuleMetadatas.First(mm => mm.FullName == scriptDirectoryPath.Item1);

                var fullPath = VirtualPath.Concat(metadata.GetModuleVirtualPath(ConfigProvider.Value), scriptDirectoryPath.Item2);

                scriptBundle.IncludeDirectory(fullPath, scriptDirectoryPath.Item3, true);
            }
            BundleTable.Bundles.Add(scriptBundle);
        }
    }
}