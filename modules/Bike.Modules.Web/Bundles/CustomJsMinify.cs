﻿namespace Bike.Modules.Web.Bundles
{
    using System;
    using System.Web.Optimization;

    using Microsoft.Ajax.Utilities;

    public class CustomJsMinify : IItemTransform
    {
        public string Process(string includedVirtualPath, string input)
        {
            if (includedVirtualPath.EndsWith("min.js", StringComparison.OrdinalIgnoreCase))
            {
                return input;
            }

            Minifier minifier = new Minifier();
            var codeSettings = new CodeSettings();
            codeSettings.EvalTreatment = EvalTreatment.MakeImmediateSafe;
            codeSettings.PreserveImportantComments = false;

            string str = minifier.MinifyJavaScript(input, codeSettings);

            if (minifier.ErrorList.Count > 0)
            {
                return "/* " + string.Concat(minifier.Errors) + " */";
            }

            return str;
        }
    }
}