﻿namespace Bike.Modules.Web.VirtualPathProvider
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Hosting;

    using Bike.Common.Interfaces;
    using Bike.Modules.Web.Helpers;
    using Bike.Modules.Web.Providers.Interfaces;
    using Bike.Modules.Web.Utils;
    using Bike.Modules.Web.VirtualPathProvider.Interfaces;

    public class ModularityPathProvider : VirtualPathProvider,
                                          IModularityPathProvider
    {
        private readonly Lazy<IEnumerable<KeyValuePair<string, string>>> _modulePaths;

        public IEnumerable<IModuleMetadata> ModuleMetadatas { get; set; }

        public Lazy<IWebRecipeProvider> WebConfigProvider { get; set; }

        public ModularityPathProvider()
        {
            _modulePaths =
                new Lazy<IEnumerable<KeyValuePair<string, string>>>(
                    () =>
                    ModuleMetadatas.Select(
                        m => new KeyValuePair<string, string>(m.GetModuleVirtualPath(WebConfigProvider.Value).ToLower(), m.FolderPath.ToLower())));
        }

        public override bool DirectoryExists(string virtualDir)
        {
            return Directory.Exists(GetAbsolutePath(virtualDir));
        }

        public override bool FileExists(string virtualPath)
        {
            var extension = VirtualPathUtility.GetExtension(virtualPath);
            if (string.IsNullOrEmpty(extension))
            {
                return false;
            }

            return !string.IsNullOrEmpty(GetAbsolutePath(virtualPath));
        }

        public override VirtualDirectory GetDirectory(string virtualDir)
        {
            return new ModularityVirtualDirectory(virtualDir, GetAbsolutePath(virtualDir));
        }

        public override VirtualFile GetFile(string virtualPath)
        {
            return new ModularityVirtualFile(virtualPath, GetAbsolutePath(virtualPath));
        }

        #region Закрытые методы

        private string GetAbsolutePath(string virtualDir)
        {
            return VirtualPath.GetAbsolutePath(virtualDir, _modulePaths.Value);
        }

        #endregion
    }
}