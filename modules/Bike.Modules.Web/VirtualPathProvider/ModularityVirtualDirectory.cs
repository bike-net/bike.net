﻿namespace Bike.Modules.Web.VirtualPathProvider
{
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.Hosting;

    using Seterlund.CodeGuard;

    public class ModularityVirtualDirectory : VirtualDirectory
    {
        private readonly DirectoryInfo _directoryInfo;

        public override IEnumerable Directories
        {
            get { return GetDirectories(); }
        }

        public override IEnumerable Files
        {
            get { return GetFiles(); }
        }

        public override IEnumerable Children
        {
            get
            {
                return GetDirectories()
                    .Cast<VirtualFileBase>()
                    .Union(GetFiles());
            }
        }

        public ModularityVirtualDirectory(string virtualPath, string absolutePath) : base(virtualPath)
        {
            Guard.That(absolutePath)
                 .IsNotNullOrWhiteSpace();

            _directoryInfo = new DirectoryInfo(absolutePath);
        }

        #region Закрытые методы

        private IEnumerable<ModularityVirtualDirectory> GetDirectories()
        {
            return _directoryInfo.EnumerateDirectories()
                                 .Select(d => new ModularityVirtualDirectory(Utils.VirtualPath.Concat(VirtualPath, d.Name), d.FullName));
        }

        private IEnumerable<ModularityVirtualFile> GetFiles()
        {
            return _directoryInfo.EnumerateFiles()
                                 .Select(f => new ModularityVirtualFile(Utils.VirtualPath.Concat(VirtualPath, f.Name), f.FullName));
        }

        #endregion
    }
}