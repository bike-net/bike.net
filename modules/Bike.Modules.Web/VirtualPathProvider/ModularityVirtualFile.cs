﻿namespace Bike.Modules.Web.VirtualPathProvider
{
    using System.IO;
    using System.Web.Hosting;

    public class ModularityVirtualFile : VirtualFile
    {
        private readonly FileInfo _fileInfo;

        public override bool IsDirectory
        {
            get { return _fileInfo.Attributes.HasFlag(FileAttributes.Directory); }
        }

        public override string Name
        {
            get { return _fileInfo.Name; }
        }

        public ModularityVirtualFile(string virtualpath, string absolutePath) : base(virtualpath)
        {
            _fileInfo = new FileInfo(absolutePath);
        }

        public override Stream Open()
        {
            return _fileInfo.OpenRead();
        }
    }
}