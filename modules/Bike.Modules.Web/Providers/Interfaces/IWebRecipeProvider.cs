﻿namespace Bike.Modules.Web.Providers.Interfaces
{
    public interface IWebRecipeProvider
    {
        /// <summary>Путь до минифицированных скриптов</summary>
        string JsBundlePath { get; }

        /// <summary>Пусть до минифицированных стилей</summary>
        string CssBundlePath { get; }

        /// <summary>Виртуальный путь до модулей</summary>
        string ModulesVirtualPath { get; }
    }
}