﻿namespace Bike.Modules.Web.Providers
{
    using Bike.Common.Providers;
    using Bike.Modules.Web.Providers.Interfaces;

    public class WebRecipeProvider : BaseRecipeProvider,
                                     IWebRecipeProvider
    {
        /// <summary>Путь до минифицированных скриптов</summary>
        public string ModulesVirtualPath
        {
            get { return Resipe.GetSettings("modulesVirtualPath", WebModule.WebModuleName, "Modules"); }
        }

        /// <summary>Пусть до минифицированных стилей</summary>
        public string CssBundlePath
        {
            get { return Resipe.GetSettings("cssBundlePath", WebModule.WebModuleName, @"~/bundles/css"); }
        }

        /// <summary>Виртуальный путь до модулей</summary>
        public string JsBundlePath
        {
            get { return Resipe.GetSettings("jsBundlePath", WebModule.WebModuleName, @"~/bundles/script"); }
        }
    }
}