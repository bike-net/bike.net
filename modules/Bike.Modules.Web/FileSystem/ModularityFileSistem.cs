﻿namespace Bike.Modules.Web.FileSystem
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using Bike.Common.Interfaces;
    using Bike.Modules.Web.Helpers;
    using Bike.Modules.Web.Providers.Interfaces;
    using Bike.Modules.Web.Utils;

    using Microsoft.Owin.FileSystems;

    using Seterlund.CodeGuard;

    public class ModularityFileSistem : IFileSystem
    {
        private readonly List<KeyValuePair<string, string>> _modulePaths;

        public ModularityFileSistem(IList<IModuleMetadata> moduleMetadatas, IWebRecipeProvider recipeProvider)
        {
            Guard.That(moduleMetadatas)
                 .IsNotNull();

            _modulePaths = moduleMetadatas.Select(
                m => new KeyValuePair<string, string>(
                         m.GetModuleVirtualPath(recipeProvider)
                          .ToLower(),
                         m.FolderPath.ToLower()))
                                          .ToList();
        }

        public bool TryGetDirectoryContents(string subpath, out IEnumerable<IFileInfo> contents)
        {
            contents = Enumerable.Empty<IFileInfo>();
            return false;
        }

        public bool TryGetFileInfo(string subpath, out IFileInfo fileInfo)
        {
            fileInfo = null;

            var exnetion = VirtualPathUtility.GetExtension(subpath);
            if (string.IsNullOrEmpty(exnetion))
            {
                return false;
            }

            var absolutePath = VirtualPath.GetAbsolutePath(subpath, _modulePaths);

            var isExist = !string.IsNullOrEmpty(absolutePath);

            if (isExist)
            {
                fileInfo = new ModularityFileInfo(absolutePath);
            }

            return isExist;
        }
    }
}