﻿namespace Bike.Modules.Web.FileSystem
{
    using System;
    using System.IO;

    public class ModularityFileInfo : IFileInfo
    {
        private readonly FileInfo _fileInfo;

        public long Length
        {
            get { return _fileInfo.Length; }
        }

        public string PhysicalPath
        {
            get { return _fileInfo.FullName; }
        }

        public string Name
        {
            get { return _fileInfo.Name; }
        }

        public DateTime LastModified
        {
            get { return _fileInfo.LastWriteTime; }
        }

        public bool IsDirectory
        {
            get { return _fileInfo.Attributes.HasFlag(FileAttributes.Directory); }
        }

        public ModularityFileInfo(string path)
        {
            _fileInfo = new FileInfo(path);
        }

        public Stream CreateReadStream()
        {
            return _fileInfo.Exists
                       ? _fileInfo.OpenRead()
                       : null;
        }
    }
}