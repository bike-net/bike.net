﻿namespace Bike.Modules.Web.Context
{
    using System.Web;

    using Bike.Common.Interfaces;
    using Bike.Modules.Web.Providers.Interfaces;
    using Bike.Modules.Web.Utils;

    public class WebBikeContext : IBikeContext
    {
        public IWebRecipeProvider WebRecipeProvider { get; set; }

        public string ModulesVirtualPath
        {
            get { return WebRecipeProvider.ModulesVirtualPath; }
        }

        public string ConcatPath(params string[] paths)
        {
            return VirtualPath.Concat(paths);
        }

        public string MapPath(string relativePath)
        {
            return HttpContext.Current.Server.MapPath(relativePath);
        }
    }
}