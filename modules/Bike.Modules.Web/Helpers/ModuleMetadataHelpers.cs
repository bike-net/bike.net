﻿namespace Bike.Modules.Web.Helpers
{
    using System.Globalization;

    using Bike.Common.Interfaces;
    using Bike.Modules.Web.Providers.Interfaces;
    using Bike.Modules.Web.Utils;

    public static class ModuleMetadataHelpers
    {
        public static string GetModuleVirtualPath(this IModuleMetadata metadata, IWebRecipeProvider recipeProvider)
        {
            return VirtualPath.Concat(VirtualPath.VirtualPathTilde.ToString(CultureInfo.InvariantCulture), recipeProvider.ModulesVirtualPath, metadata.ShortName);
        }
    }
}