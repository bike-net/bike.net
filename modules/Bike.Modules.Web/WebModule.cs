﻿using Bike.Contracts.Web;

namespace Bike.Modules.Web
{
    using System;

    using Bike.Common.Attributes;
    using Bike.Common.Interfaces;
    using Bike.Common.Ioc;
    using Bike.Modules.Web.Bundles;
    using Bike.Modules.Web.Context;
    using Bike.Modules.Web.FileSystem;
    using Bike.Modules.Web.Providers;
    using Bike.Modules.Web.Providers.Interfaces;
    using Bike.Modules.Web.VirtualPathProvider;
    using Bike.Modules.Web.VirtualPathProvider.Interfaces;

    [BikeModule(WebModuleName, "0.0.1")]
    [BikeExportContract("Contracts:Web", "[0.0.0,)")]
    public class WebModule : IBikeModule
    {
        public const string WebModuleName = "Modules:Web";

        public IBikeDependencyRegistrator BikeDependencyRegistrator { get; set; }

        public Lazy<IModularityPathProvider> ModularityPathProvider { get; set; }

        public Lazy<IBundleRegistration> BundleRegistration { get; set; }

        public Lazy<IFileSystem> FileSystem { get; set; }

        public Lazy<IAppBuilder> AppBuilder { get; set; }

        public void Initializing()
        {
            BundleTable.VirtualPathProvider = (System.Web.Hosting.VirtualPathProvider)ModularityPathProvider.Value;

            BundleRegistration.Value.Register();

            AppBuilder.Value.UseStaticFiles(
                new StaticFileOptions
                {
                    FileSystem = FileSystem.Value
                });

            AppBuilder.Value.UseStageMarker(PipelineStage.MapHandler);
        }

        public void RegisterDependency()
        {
            BikeDependencyRegistrator.RegisterAsSingleton<IWebRecipeProvider, WebRecipeProvider>();
            BikeDependencyRegistrator.RegisterAsSingleton<IFileSystem, ModularityFileSistem>();
            BikeDependencyRegistrator.RegisterAsSingleton<IBundleRegistration, BikeBundleRegistration>();
            BikeDependencyRegistrator.RegisterAsSingleton<IModularityPathProvider, ModularityPathProvider>();
            BikeDependencyRegistrator.RegisterAsSingleton<IBikeContext, WebBikeContext>();
        }
    }
}