﻿namespace Bike.Modules.Web.Utils
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public static class VirtualPath
    {
        public const char VirtualPathSeparator = '/';

        public const char VirtualPathTilde = '~';

        public static string Concat(params string[] paths)
        {
            var stringBuilder = new StringBuilder();

            for (var i = 0; i < paths.Length; i++)
            {
                var trimPath = paths[i];
                if (i > 0)
                {
                    trimPath = trimPath.Trim(VirtualPathTilde);
                }

                trimPath = trimPath.Trim(VirtualPathSeparator);
                stringBuilder.Append(trimPath);

                if (i != paths.Length - 1)
                {
                    stringBuilder.Append(VirtualPathSeparator);
                }
            }

            return stringBuilder.ToString();
        }

        public static string GetAbsolutePath(string virtualPath, IEnumerable<KeyValuePair<string, string>> paths)
        {
            var lowerPath = virtualPath.ToLower()
                                       .TrimStart(VirtualPathTilde);

            var modulePath = paths.FirstOrDefault(m => StartWith(lowerPath, m.Key));

            return modulePath.Key != null
                       ? ReplaceVirtualPath(lowerPath.Replace(modulePath.Key.TrimStart(VirtualPathTilde), modulePath.Value.TrimStart(VirtualPathTilde)))
                       : string.Empty;
        }

        public static string ReplaceVirtualPath(string path)
        {
            return path.Replace(VirtualPathSeparator, '\\');
        }

        public static bool StartWith(string origin, string part)
        {
            var lowerOrigin = origin.ToLower()
                                    .TrimStart(VirtualPathTilde);
            var lowerPart = part.ToLower()
                                .TrimStart(VirtualPathTilde);

            return lowerOrigin.StartsWith(lowerPart);
        }
    }
}