﻿namespace Bike.Modules.PhysicalFileSystem
{
    using System;
    using System.IO;

    using Bike.Common.Interfaces;

    internal static class Utils
    {
        public static string GetPhysicalPath(IBikeContext bikeContext, string initPath, string relativePath)
        {
            var fullPath = bikeContext.ConcatPath(initPath, relativePath);

            return bikeContext.MapPath(fullPath);
        }

        public static string MakeRelativePath(IBikeContext bikeContext, string toPath)
        {
            var fromUri = new Uri(bikeContext.MapPath("/"));
            var toUri = new Uri(toPath);

            if (fromUri.Scheme != toUri.Scheme)
            {
                return toPath; // path can't be made relative.
            }

            var relativeUri = fromUri.MakeRelativeUri(toUri);
            var relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            if (toUri.Scheme.ToUpperInvariant() == "FILE")
            {
                relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            }

            return relativePath;
        }
    }
}