﻿namespace Bike.Modules.PhysicalFileSystem
{
    using Bike.Common.Attributes;
    using Bike.Common.Interfaces;
    using Bike.Common.Ioc;
    using Bike.Contracts.FileSystem;
    using Bike.Modules.PhysicalFileSystem.Interfaces;
    using Bike.Modules.PhysicalFileSystem.Providers;

    [BikeModule(PhysicalFileSystemName, "0.0.1")]
    [BikeExportContract("Contracts:FileSystem", "[0.0.0,)")]
    public class PhysicalFileSystemModule : IBikeModule
    {
        public const string PhysicalFileSystemName = "Modules:PhysicalFileSystem";

        public IBikeDependencyRegistrator BikeDependencyRegistrator { get; set; }

        public void Initializing()
        {
        }

        public void RegisterDependency()
        {
            BikeDependencyRegistrator.RegisterAsSingleton<IFileService, PhysicalFileService>();
            BikeDependencyRegistrator.RegisterAsSingleton<IDirectoryService, PhysicalDirectoryService>();
            BikeDependencyRegistrator.RegisterAsSingleton<IPhysicalFileSystemConfig, PhysicalFileSystemRecipeProvider>();
        }
    }
}