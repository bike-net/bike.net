namespace Bike.Modules.PhysicalFileSystem.Entities
{
    using System.IO;

    public class TempStream : FileStream
    {
        private readonly string _path;

        public TempStream(string path, FileMode mode) : base(path, mode)
        {
            _path = path;
        }

        public TempStream(string path, FileMode mode, FileAccess access) : base(path, mode, access)
        {
            _path = path;
        }

        public TempStream(string path, FileMode mode, FileAccess access, FileShare share) : base(path, mode, access, share)
        {
            _path = path;
        }

        public TempStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize) : base(path, mode, access, share, bufferSize)
        {
            _path = path;
        }

        public TempStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, FileOptions options)
            : base(path, mode, access, share, bufferSize, options)
        {
            _path = path;
        }

        public TempStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, bool useAsync) : base(path, mode, access, share, bufferSize, useAsync)
        {
            _path = path;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            File.Delete(_path);
        }
    }
}