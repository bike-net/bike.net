﻿namespace Bike.Modules.PhysicalFileSystem.Entities
{
    using System;
    using System.IO;

    public class TempDirectoryProvider : IDisposable
    {
        private readonly string _path;

        public TempDirectoryProvider(string path)
        {
            _path = path;
        }

        public void Dispose()
        {
            DisposeProtected();
        }

        protected virtual void DisposeProtected()
        {
            Directory.Delete(_path);
        }
    }
}