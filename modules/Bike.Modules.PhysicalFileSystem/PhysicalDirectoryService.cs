﻿namespace Bike.Modules.PhysicalFileSystem
{
    using System;
    using System.IO;

    using Bike.Common.Interfaces;
    using Bike.Contracts.FileSystem;
    using Bike.Modules.PhysicalFileSystem.Entities;
    using Bike.Modules.PhysicalFileSystem.Interfaces;

    public class PhysicalDirectoryService : IDirectoryService
    {
        public IPhysicalFileSystemConfig PhysicalFileSystemConfig { get; set; }

        public IBikeContext BikeContext { get; set; }

        public void Create(string relativePath)
        {
            var physicalPath = Utils.GetPhysicalPath(BikeContext, PhysicalFileSystemConfig.InitialPath, relativePath);

            Directory.CreateDirectory(physicalPath);
        }

        public IDisposable CreateTempDirectory(out string relativePath)
        {
            var physicalPath = Utils.GetPhysicalPath(BikeContext, PhysicalFileSystemConfig.InitialPath, "/");

            var physicalTempPath = Path.Combine(physicalPath, PhysicalFileSystemConfig.TempDirectoryPath, Path.GetRandomFileName());

            Directory.CreateDirectory(physicalTempPath);

            relativePath = Utils.MakeRelativePath(BikeContext, physicalTempPath);

            return new TempDirectoryProvider(physicalTempPath);
        }

        public void Delete(string relativePath)
        {
            var physicalPath = Utils.GetPhysicalPath(BikeContext, PhysicalFileSystemConfig.InitialPath, relativePath);

            Directory.Delete(physicalPath);
        }

        public bool IsExist(string relativePath)
        {
            var physicalPath = Utils.GetPhysicalPath(BikeContext, PhysicalFileSystemConfig.InitialPath, relativePath);

            return Directory.Exists(physicalPath);
        }
    }
}