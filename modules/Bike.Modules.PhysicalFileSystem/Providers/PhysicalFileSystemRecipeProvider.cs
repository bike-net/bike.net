﻿namespace Bike.Modules.PhysicalFileSystem.Providers
{
    using Bike.Common.Providers;
    using Bike.Modules.PhysicalFileSystem.Interfaces;

    public class PhysicalFileSystemRecipeProvider : BaseRecipeProvider,
                                                    IPhysicalFileSystemConfig
    {
        public string InitialPath
        {
            get { return Resipe.GetSettings("InitialPath", PhysicalFileSystemModule.PhysicalFileSystemName, "Files"); }
        }

        public string TempDirectoryPath
        {
            get { return Resipe.GetSettings("TempDirectory", PhysicalFileSystemModule.PhysicalFileSystemName, "Temp"); }
        }
    }
}