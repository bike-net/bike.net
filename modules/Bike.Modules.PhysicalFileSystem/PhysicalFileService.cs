﻿namespace Bike.Modules.PhysicalFileSystem
{
    using System.IO;

    using Bike.Common.Interfaces;
    using Bike.Contracts.FileSystem;
    using Bike.Modules.PhysicalFileSystem.Entities;
    using Bike.Modules.PhysicalFileSystem.Interfaces;

    public class PhysicalFileService : IFileService
    {
        public IDirectoryService DirectoryService { get; set; }

        public IPhysicalFileSystemConfig PhysicalFileSystemConfig { get; set; }

        public IBikeContext BikeContext { get; set; }

        public Stream CreateTempFile(string relativeFolderPath)
        {
            var physicalPath = Utils.GetPhysicalPath(BikeContext, PhysicalFileSystemConfig.InitialPath, relativeFolderPath);

            if (!DirectoryService.IsExist(relativeFolderPath))
            {
                DirectoryService.Create(relativeFolderPath);
            }

            var fullPath = Path.Combine(physicalPath, Path.GetRandomFileName());

            return new TempStream(fullPath, FileMode.CreateNew);
        }

        public bool IsExist(string relativePath)
        {
            var physicalPath = Utils.GetPhysicalPath(BikeContext, PhysicalFileSystemConfig.InitialPath, relativePath);

            return File.Exists(physicalPath);
        }

        public Stream OpenFile(string relativePath, FileMode fileMode, FileAccess fileAccess, FileShare fileShare)
        {
            var physicalPath = Utils.GetPhysicalPath(BikeContext, PhysicalFileSystemConfig.InitialPath, relativePath);

            return new FileStream(physicalPath, fileMode, fileAccess, fileShare, 4096, true);
        }
    }
}