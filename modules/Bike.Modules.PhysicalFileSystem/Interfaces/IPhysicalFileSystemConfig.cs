﻿namespace Bike.Modules.PhysicalFileSystem.Interfaces
{
    public interface IPhysicalFileSystemConfig
    {
        string InitialPath { get; }

        string TempDirectoryPath { get; }
    }
}