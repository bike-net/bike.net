﻿namespace Bike.Modules.DotNetZip
{
    using Bike.Common.Attributes;
    using Bike.Common.Interfaces;
    using Bike.Common.Ioc;
    using Bike.Contracts.Zip;

    [BikeModule(BikeModulesDonNetZip, "0.0.1")]
    [BikeImportContract("Contracts:FileSystem", "[0.0.0,)")]
    [BikeExportContract("Contracts:Zip", "[0.0.0,)")]
    public class BikeDotNetZipModule : IBikeModule
    {
        private const string BikeModulesDonNetZip = "Modules:DonNetZip";

        public IBikeDependencyRegistrator BikeDependencyRegistrator { get; set; }

        public void Initializing()
        {
        }

        public void RegisterDependency()
        {
            BikeDependencyRegistrator.RegisterAsTransient<IZipService, ZipService>();
        }
    }
}