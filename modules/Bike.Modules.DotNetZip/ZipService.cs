﻿namespace Bike.Modules.DotNetZip
{
    using System.IO;

    using Bike.Contracts.FileSystem;
    using Bike.Contracts.Zip;

    using Ionic.Zip;

    public class ZipService : IZipService
    {
        public IFileService FileService { get; set; }

        public Stream ZipFiles(params string[] relativePaths)
        {
            using (var zip = new ZipFile())
            {
                foreach (var relativePath in relativePaths)
                {
                    var entryName = Path.GetFileName(relativePath);

                    var file = FileService.OpenFile(relativePath, FileMode.Open, FileAccess.Read, FileShare.None);
                    zip.AddEntry(entryName, file);
                }

                var outputStream = FileService.CreateTempFile("/Temp");
                zip.Save(outputStream);
                outputStream.Flush();

                outputStream.Seek(0, SeekOrigin.Begin);

                return outputStream;
            }
        }
    }
}