﻿namespace Bike.CoreTest.Utils
{
    using System.Linq;

    using Bike.Core.Utils;
    using Bike.TestFramework;

    using NuGet.Versioning;

    using Xunit;

    public class NugetUtilsTest : BaseTest
    {
        /// <summary>Провекра формирования периода пакетов</summary>
        [Theory]
        [InlineData("[1.0.0,2.0.0)", "[1.5.0,2.0.0]", "[1.5.0, 2.0.0)")]
        [InlineData("(1.0.0,2.0.0]", "[0.5.0,2.0.0]", "(1.0.0, 2.0.0]")]
        public void CombineVersionRange(string firstVersion, string secondVersion, string result)
        {
            var versions = new[] { VersionRange.Parse(firstVersion), VersionRange.Parse(secondVersion) }.AsEnumerable();

            var combineVersion = versions.CombineVersionRange();
            Assert.Equal(result, combineVersion.ToString());
        }
    }
}