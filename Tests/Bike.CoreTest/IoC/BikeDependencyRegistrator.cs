﻿namespace Bike.CoreTest.IoC
{
    using Bike.Common.Interfaces;
    using Bike.Core.IoC;
    using Bike.TestFramework;
    using Bike.TestFramework.Extentions;

    using Moq;

    using Xunit;


    public class BikeDependencyRegistratorTest : BaseTest
    {
        [Fact]
        public void RegisterInstance()
        {
            SetupTest();
            var registrator = this.ResolveService<BikeDependencyRegistrator>();
            var metadata = Mock.Of<IModuleMetadata>();


            registrator.RegisterInstance<IModuleMetadata>(metadata);

            Assert.StrictEqual(metadata, Container.Resolve<IModuleMetadata>());
        }

        private void SetupTest()
        {
            BikeDependencyRegistrator dependencyRegistrator = new BikeDependencyRegistrator(Container);
            this.RegisterService(dependencyRegistrator);
        }
    }
}
