﻿namespace Bike.CoreTest
{
    using System.IO;
    using System.Text;

    using Bike.Core.Recipes;

    using Xunit;

    public class RecipeTest
    {
        [Theory]
        [InlineData("{dependencies:[{moduleName:'123'}]}")]
        [InlineData("{dependencies:[{ModuleName:'123',Version:'[123.123)'}]}")]
        public void DeserializeDependencies(string recipeSource)
        {
            var recipe = LoadRecipe(recipeSource);
            Assert.NotEmpty(recipe.GetDependencies());
        }

        [Theory]
        [InlineData("{dependencies:[]}")]
        [InlineData("{}")]
        public void DeserializeEmptyDependencies(string recipeSource)
        {
            var recipe = LoadRecipe(recipeSource);
            Assert.NotNull(recipe);
            Assert.NotNull(recipe.GetDependencies());
        }

        [Theory]
        [InlineData("{implementationOfContracts:[]}")]
        [InlineData("{}")]
        public void DeserializeEmptyImplementationOfContracts(string recipeSource)
        {
            var recipe = LoadRecipe(recipeSource);

            Assert.NotNull(recipe);
            Assert.NotNull(recipe.GetImplimentationOfContracts());
        }

        [Theory]
        [InlineData("{settings:{}}")]
        [InlineData("{}")]
        public void DeserializeEmptySettings(string recipeSource)
        {
            var recipe = LoadRecipe(recipeSource);

            Assert.NotNull(recipe);
            Assert.Equal("123", recipe.GetSettings("asda", "temp", "123"));
        }

        [Theory]
        [InlineData("{implimentationOfContracts:[{ModuleName:'123'}]}")]
        [InlineData("{implimentationOfContracts:[{ContractName:'123',ModuleName:'123'}]}")]
        public void DeserializeImplementationOfContracts(string recipeSource)
        {
            var recipe = LoadRecipe(recipeSource);
            Assert.NotNull(recipe);
            Assert.NotEmpty(recipe.GetImplimentationOfContracts());
        }

        [Theory]
        [InlineData("{settings:{ModuleName:{Value:'123'}}}")]
        [InlineData("{settings:{ContractName:'123',ModuleName:'123'}}")]
        public void DeserializeSettings(string recipeSource)
        {
            var recipe = LoadRecipe(recipeSource);
            Assert.NotNull(recipe);
            Assert.NotEmpty(recipe.GetSettings("Value", "ModuleName", ""));
        }

        [Fact]
        public void SerializeSettings()
        {
            var recipe = new Recipe();
            recipe.SetSettings("Test", "123", "qwe");

            using (var stream = new MemoryStream())
            {
                recipe.Save(stream);

                stream.Seek(0, SeekOrigin.Begin);

                recipe.Load(stream);

                Assert.Equal(recipe.GetSettings("Test", "123", ""), "qwe");
            }
        }

        #region Закрытые методы

        private static Recipe LoadRecipe(string recipeSource)
        {
            Recipe recipe;
            using (Stream stream = new MemoryStream(Encoding.UTF8.GetBytes(recipeSource)))
            {
                recipe = new Recipe();
                recipe.Load(stream);
            }
            return recipe;
        }

        #endregion
    }
}