﻿namespace Bike.CoreTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Bike.Common.Interfaces;
    using Bike.Common.Recipes;
    using Bike.Core.Modules.Resolvers.Interfaces;
    using Bike.Core.Modules.TreeBuilders;
    using Bike.Core.Recipes;
    using Bike.Core.Store.Interfaces;
    using Bike.TestFramework;
    using Bike.TestFramework.Extentions;

    using Moq;

    using NuGet.Versioning;

    using Xunit;

    public class ModuleTreeBuilderTest : BaseTest
    {
        /// <summary>Построение дерева без зависимостей</summary>
        [Theory]
        [InlineData("Module1", "[0.0.0,)", "Module1", "1.0.0")]
        [InlineData("Module1", "[0.0.0,)", "module1", "1.0.0")]
        [InlineData("module1", "[0.0.0,)", "Module1", "1.0.0")]
        [InlineData("Module1", "[0.0.0,)", "module1", "1.0.0")]
        public async Task BuildTree(string nameInRecipe, string versionInRecipe, string nameInRepo, string versionInRepo)
        {
            var mLocalModule = new Mock<IModuleMetadata>();
            mLocalModule.SetupGet(metadata => metadata.FullName)
                        .Returns(nameInRepo);

            mLocalModule.SetupGet(metadata => metadata.Version)
                        .Returns(NuGetVersion.Parse(versionInRepo));

            var versionInRec = VersionRange.Parse(versionInRecipe);

            var recipeDep = new RecipeDependencyItem
                            {
                                ModuleName = nameInRecipe,
                                Version = versionInRecipe
                            };

            SetupTest(nameInRecipe, versionInRec, new[] { mLocalModule.Object }, new[] { recipeDep });

            var builder = this.ResolveService<ModuleTreeBuilder>();
            var tree = await builder.BuildTree();

            Assert.Equal(1, tree.VertexCount);
            Assert.True(tree.Vertices.Any(v => v.ModuleName == nameInRecipe));
        }

        ///// <summary>Построение дерева с зависимостями для пакета Microsoft.AspNet.Mvc</summary>
        //[Fact]
        //public async Task BuildTreeAspNetMvc()
        //{
        //    var recipeDep = new[] { BuildRecipeDependency("Microsoft.AspNet.Mvc", "5.2.3") };

        //    var mModularRecipeProvider = new Mock<IModularRecipeProvider>();
        //    mModularRecipeProvider.Setup(provider => provider.StoreUrl)
        //                          .Returns(() => "http://www.nuget.org/api/v2/");

        //    var resolver = new StoreDependencyResolver
        //                   {
        //                       RecipeProvider = mModularRecipeProvider.Object
        //                   };

        //    var mRecipe = new Mock<IRecipe>();
        //    mRecipe.Setup(recipe => recipe.GetDependencies())
        //           .Returns(() => recipeDep);
        //    var builder = new ModuleTreeBuilder
        //                  {
        //                      StoreDependencyResolver = resolver,
        //                      Recipe = mRecipe.Object
        //                  };

        //    await builder.BuildTree();
        //}

        /// <summary>Построение дерева без зависимостей с разными версиями</summary>
        //[Fact]
        //public async Task BuildTreeDifferentVersion()
        //{
        //    const string module1 = "TestModule 1";
        //    const string module2 = "TestModule 2";

        //    var storeDep = new[]
        //                   {
        //                       BuildPackageDependencyInfo(module1, "0.1"),
        //                       BuildPackageDependencyInfo(module1, "0.2"),
        //                       BuildPackageDependencyInfo(module2, "0.2", new Tuple<string, string>(module1, "0.2")),
        //                       BuildPackageDependencyInfo(module2, "0.1", new Tuple<string, string>(module1, "0.1"))
        //                   };

        //    var recipeDep = new[] { BuildRecipeDependency(module2, "0.1") };

        //    Mock<IStoreDependencyResolver> mStore;
        //    Mock<IRecipe> mRecipe;
        //    var builder1 = SetupTest(module2, storeDep, recipeDep, out mStore, out mRecipe);
        //    var builder = builder1;

        //    var tree = await builder.BuildTree();

        //    Assert.True(tree.ContainsEdge(new ModuleTreeVertex(module1), new ModuleTreeVertex(module2)));

        //    Edge<ModuleTreeVertex> edge;
        //    Assert.True(tree.TryGetEdge(new ModuleTreeVertex(module1), new ModuleTreeVertex(module2), out edge));
        //}

        ///// <summary>Построение дерева с зависимостями</summary>
        //[Theory]
        //[InlineData("1.0", "1.0", "1.0")]
        //[InlineData("1.1", "1.0", "[1.0,)")]
        //public async Task BuildTreeWithDependencies(string firstModuleVersion, string secondModuleVersion, string recipeVersion)
        //{
        //    const string module1 = "TestModule 1";
        //    const string module2 = "TestModule 2";

        //    var storeDep = new[]
        //                   {
        //                       BuildPackageDependencyInfo(module1, firstModuleVersion),
        //                       BuildPackageDependencyInfo(module2, secondModuleVersion, new Tuple<string, string>(module1, firstModuleVersion))
        //                   };

        //    var recipeDep = new[] { BuildRecipeDependency(module2, recipeVersion) };

        //    Mock<IStoreDependencyResolver> mStore;
        //    Mock<IRecipe> mRecipe;
        //    var builder = SetupTest(module2, storeDep, recipeDep, out mStore, out mRecipe);

        //    var tree = await builder.BuildTree();

        //    Assert.True(tree.ContainsEdge(new ModuleTreeVertex(module1), new ModuleTreeVertex(module2)));

        //    Edge<ModuleTreeVertex> edge;
        //    Assert.True(tree.TryGetEdge(new ModuleTreeVertex(module1), new ModuleTreeVertex(module2), out edge));
        //}

        #region Закрытые методы

        private void SetupTest(string moduleName, VersionRange version, IEnumerable<IModuleMetadata> localModules, ICollection<IRecipeDependencyItem> recipeDependencies)
        {
            var mStore = new Mock<ILocalModuleResolver>();
            mStore.Setup(store => store.IsExist(moduleName, version))
                  .Returns<string, VersionRange>((n, v) => Task.FromResult(true));
            mStore.Setup(store => store.GetModuleMetadata(moduleName, version))
                  .Returns<string, VersionRange>((n, v) => Task.FromResult(localModules.SingleOrDefault(m => m.FullName.Equals(n, StringComparison.InvariantCultureIgnoreCase) && v.Satisfies(m.Version))));

            var mRecipe = new Mock<IRecipe>();
            mRecipe.Setup(recipe => recipe.GetDependencies())
                   .Returns(() => recipeDependencies);

            this.RegisterMockWithService(mStore);
            this.RegisterMockWithService(mRecipe);
            this.RegisterService<ModuleTreeBuilder, ModuleTreeBuilder>();
        }

        #endregion
    }
}