﻿

namespace Bike.TestFramework.AFixture
{
    using System.Reflection;

    using AutoFixture;
    using AutoFixture.AutoMoq;
    using AutoFixture.Kernel;

    public class AutoPopulatedMoqPropertiesCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customizations.Add(new PropertiesPostprocessor(new MockPostprocessor(new MethodInvoker(new MockConstructorQuery()))));
            fixture.ResidueCollectors.Add(new Postprocessor(new MockRelay(), new AutoPropertiesCommand(new PropertiesOnlySpecification())));
        }

        private class PropertiesOnlySpecification : IRequestSpecification
        {
            public bool IsSatisfiedBy(object request)
            {
                return request is PropertyInfo;
            }
        }
    }
}