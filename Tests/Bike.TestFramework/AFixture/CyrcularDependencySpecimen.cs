﻿
namespace Bike.TestFramework.AFixture
{
    using System.Reflection;
    using AutoFixture.Kernel;

    public class CyrcularDependencySpecimen : ISpecimenBuilder

    {
        /// <summary>Creates a new specimen based on a request.</summary>
        /// <param name="request">The request that describes what to create.</param>
        /// <param name="context">A context that can be used to create other specimens.</param>
        /// <returns>The requested specimen if possible; otherwise a <see cref="T:Ploeh.AutoFixture.Kernel.NoSpecimen" /> instance.</returns>
        /// <remarks>
        ///     <para>The <paramref name="request" /> can be any object, but will often be a
        ///         <see cref="T:System.Type" /> or other <see cref="T:System.Reflection.MemberInfo" /> instances.</para>
        ///     <para>Note to implementers: Implementations are expected to return a
        ///         <see cref="T:Ploeh.AutoFixture.Kernel.NoSpecimen" /> instance if they can't satisfy the request.</para>
        /// </remarks>
        public object Create(object request, ISpecimenContext context)
        {
            var property = request as PropertyInfo;
            if (property == null)
            {
                return new NoSpecimen();
            }

            var propertyType = property.PropertyType;
            var declaringType = property.DeclaringType;

            if (propertyType == declaringType || declaringType.IsGenericType && declaringType.GetGenericArguments()[0] == propertyType)
            {
                return null;
            }

            return new NoSpecimen();
        }
    }
}