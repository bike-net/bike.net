﻿namespace Bike.TestFramework.Extentions
{
    using Castle.MicroKernel.Registration;

    using Moq;

    public static class ContainerExtentions
    {
        public static void RegisterMockWithService<TService>(this BaseTest baseTest, Mock<TService> instance) where TService : class
        {
            baseTest.Container.Register(
                Component.For<Mock<TService>>()
                         .Instance(instance),
                Component.For<TService>()
                         .UsingFactoryMethod(() => instance.Object));
        }

        public static void RegisterService<TService, TImplimentation>(this BaseTest baseTest) where TImplimentation : TService where TService : class
        {
            baseTest.Container.Register(
                Component.For<TService>()
                         .ImplementedBy<TImplimentation>());
        }

        public static void RegisterService<TService>(this BaseTest baseTest, TService instance) where TService : class
        {
            baseTest.Container.Register(
                Component.For<TService>()
                         .Instance(instance));
        }

        public static TService ResolveService<TService>(this BaseTest baseTest) where TService : class
        {
            return baseTest.Container.Resolve<TService>();
        }
    }
}