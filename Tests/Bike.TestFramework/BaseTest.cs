﻿

namespace Bike.TestFramework
{
    using Castle.Windsor;
    using AutoFixture;

    using AFixture;

    public class BaseTest
    {
        public Fixture Fixture { get; private set; }

        public IWindsorContainer Container { get; private set; }

        public BaseTest()
        {
            Container = new WindsorContainer();

            Fixture = new Fixture();
            Fixture.Customize(new AutoPopulatedMoqPropertiesCustomization());
            Fixture.Customizations.Add(new CyrcularDependencySpecimen());
        }
    }
}