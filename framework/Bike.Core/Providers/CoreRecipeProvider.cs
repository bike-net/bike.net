﻿namespace Bike.Core.Providers
{
    using Common.Providers;
    using Interfaces;

    public class CoreRecipeProvider : BaseRecipeProvider,
                                         IModularRecipeProvider
    {
        /// <summary>Пути до модулей</summary>
        public string[] ModulesPaths
        {
            get { return Resipe.GetSettings("pathsToModules", BikeApplication.BikeCoreName, new string[0]); }
        }

        /// <summary>Паттерн поиска модулей</summary>
        public string ModulesSearchPattern
        {
            get { return Resipe.GetSettings("modulesSearchPattern", BikeApplication.BikeCoreName, @"(?<=\\bin\\).*(.dll)"); }
        }

        /// <summary>Папка хоста, где будут лежат модули</summary>
        public string FolderContainsModule
        {
            get { return Resipe.GetSettings("folderContainsModule", BikeApplication.BikeCoreName, "bin"); }
        }

        /// <summary>Папка содержащая библиотеки модулей</summary>
        public string ModulesFolderBin
        {
            get { return Resipe.GetSettings("modulesFolderBin", BikeApplication.BikeCoreName, "ModulesBin"); }
        }

        /// <summary>Адрес nuget сервиса пакетов</summary>
        public string StoreUrl
        {
            get { return Resipe.GetSettings("storeUrl", BikeApplication.BikeCoreName, "https://www.myget.org/F/bike-net/"); }
        }
    }
}