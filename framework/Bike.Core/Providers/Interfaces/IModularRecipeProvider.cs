﻿namespace Bike.Core.Providers.Interfaces
{
    public interface IModularRecipeProvider
    {
        /// <summary>Пути до модулей</summary>
        string[] ModulesPaths { get; }

        /// <summary>Паттерн поиска модулей</summary>
        string ModulesSearchPattern { get; }

        /// <summary>Папка хоста, где будут лежат модули</summary>
        string FolderContainsModule { get; }

        /// <summary>Папка содержащая библиотеки модулей</summary>
        string ModulesFolderBin { get; }

        /// <summary>Адрес nuget сервиса пакетов</summary>
        string StoreUrl { get; }
    }
}