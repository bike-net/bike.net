﻿namespace Bike.Core.Store.Interfaces
{
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    using NuGet.Packaging.Core;
    using NuGet.Versioning;

    public interface IStoreDependencyResolver
    {
        Task<Stream> DownloadModule(string packageName, NuGetVersion version);

        Task<IEnumerable<PackageDependencyInfo>> GetModuleDependency(string packageName);
    }
}