namespace Bike.Core.Entities
{
    using Common.Interfaces;

    using NuGet.Versioning;

    public class ModuleDependency : IModuleDependency
    {
        /// <summary>�������� ������</summary>
        public string Name { get; set; }

        /// <summary>������ ������</summary>
        public VersionRange Version { get; set; }

        public ModuleDependency(string name, VersionRange version)
        {
            Name = name;
            Version = version;
        }
    }
}