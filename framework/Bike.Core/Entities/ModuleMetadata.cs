﻿namespace Bike.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    using BarsGroup.CodeGuard;

    using Common.Attributes;
    using Common.Interfaces;
    using Utils;

    using Mono.Cecil;

    using NuGet.Versioning;

    public class ModuleMetadata : IModuleMetadata
    {
        public const string ModuleNameFormat = @"(?:[M,m]odules|[C,c]ontracts):(.*)";

        /// <summary>Простарнство имен типа модуля</summary>
        public string ModuleTypeNamespace { get; private set; }

        /// <summary>Путь до сборки</summary>
        public string AssemblyName { get; private set; }

        /// <summary>Полный путь до папки модуля</summary>
        public string FolderPath { get; private set; }

        /// <summary>Версия модуля</summary>
        public NuGetVersion Version { get; private set; }

        /// <summary>Короткое название модуля</summary>
        public string ShortName { get; private set; }

        /// <summary>Полное название модуля</summary>
        public string FullName { get; private set; }

        /// <summary>Название модулей от которых завист модуль</summary>
        public IEnumerable<IModuleDependency> ImportModules { get; private set; }

        /// <summary>Типы контрактов которые требуются модулю</summary>
        public IEnumerable<IModuleDependency> ImportContracts { get; private set; }

        /// <summary>Тип контракта который реализует модуль</summary>
        public IModuleDependency ExportContract { get; private set; }

        /// <summary>Является ли контрактом</summary>
        public bool IsContract { get; private set; }

        public ModuleMetadata(TypeDefinition moduleType, string folderContainsModule)
        {
            FullName = GetFullModuleName(moduleType);

            ShortName = GetShortName(FullName);

            Version = GetModuleVersion(moduleType);

            AssemblyName = moduleType.Module.Assembly.Name.Name;

            ImportContracts = GetDependencies<BikeImportContractAttribute>(moduleType);

            ExportContract = GetExportContract(moduleType);

            ImportModules = GetDependencies<BikeImportModuleAttribute>(moduleType);

            FolderPath = moduleType.GetDirectoryPathForType(folderContainsModule);

            IsContract = moduleType.HasInterface<IBikeContractModule>();
        }

        #region Закрытые методы

        private static IEnumerable<IModuleDependency> GetDependencies<TAttibute>(TypeDefinition moduleType) where TAttibute : Attribute
        {
            var customAttibutes = moduleType.GetCustomAttributes<TAttibute>() ;
            if (customAttibutes == null || !customAttibutes.Any())
            {
                return Enumerable.Empty<IModuleDependency>();
            }

            return customAttibutes.Select(ToModuleDependency);
        }

        private ModuleDependency GetExportContract(TypeDefinition moduleType)
        {
            //Получение названия модуля
            var customAttibute = moduleType.GetCustomAttribute<BikeExportContractAttribute>();
            return customAttibute != null
                       ? ToModuleDependency(customAttibute)
                       : null;
        }

        private string GetFullModuleName(TypeDefinition moduleType)
        {
            var customAttibute = moduleType.GetCustomAttribute<BikeModuleAttribute>();
            Guard.That(customAttibute).IsNotNull();

            return customAttibute.ConstructorArguments[0].Value.ToString();
        }

        private NuGetVersion GetModuleVersion(TypeDefinition moduleType)
        {
            var customAttibute = moduleType.GetCustomAttribute<BikeModuleAttribute>();
            Guard.That(customAttibute).IsNotNull();

            return NuGetVersion.Parse(customAttibute.ConstructorArguments[1].Value.ToString());
        }

        private static string GetShortName(string fullName)
        {
            var reg = new Regex(ModuleNameFormat);
            var match = reg.Match(fullName);

            if (match.Length == 0)
            {
                throw new Exception(string.Format("Название модуля {0} не соответствует формату: {1}", fullName, ModuleNameFormat));
            }
            return match.Groups[1].Value;
        }

        private static ModuleDependency ToModuleDependency(CustomAttribute ca)
        {
            var moduleName = ca.ConstructorArguments[0].Value.ToString();
            var version = VersionRange.Parse(ca.ConstructorArguments[1].Value.ToString());
            return new ModuleDependency(moduleName, version);
        }

        #endregion
    }
}