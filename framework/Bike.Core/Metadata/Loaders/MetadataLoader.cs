﻿namespace Bike.Core.Metadata.Loaders
{
    using System.Collections.Generic;
    using System.Linq;

    using Common.Attributes;
    using Common.Interfaces;
    using Common.Ioc;
    using Entities;
    using Interfaces;
    using Providers.Interfaces;
    using Utils;

    using Mono.Cecil;

    public class MetadataLoader : IMetadataLoader
    {
        public IModularRecipeProvider RecipeProvider { get; set; }

        public IBikeDependencyRegistrator BikeDependencyRegistrator { get; set; }

        public IEnumerable<IModuleMetadata> LoadModuleMetadatas(IEnumerable<string> files)
        {
            var metadatas = new Dictionary<string, IModuleMetadata>();

            foreach (var file in files)
            {
                var assembly = AssemblyDefinition.ReadAssembly(file);

                var moduleType = assembly.MainModule.GetTypes()
                                         .FirstOrDefault(t => t.GetCustomAttribute<BikeModuleAttribute>() != null && t.HasInterface(typeof(IBikeModule)));

                if (moduleType == null)
                {
                    continue;
                }

                if (metadatas.ContainsKey(moduleType.FullName))
                {
                    continue;
                }

                metadatas[moduleType.FullName] = new ModuleMetadata(moduleType, RecipeProvider.FolderContainsModule);
            }

            return metadatas.Values.AsEnumerable();
        }

        public void RegisterMetadata(IModuleMetadata moduleMetadata)
        {
            BikeDependencyRegistrator.RegisterInstance<IModuleMetadata>(moduleMetadata, string.Format("metadata_{0}", moduleMetadata.FullName));
        }
      
    }
}