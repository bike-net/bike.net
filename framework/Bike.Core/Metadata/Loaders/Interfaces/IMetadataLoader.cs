namespace Bike.Core.Metadata.Loaders.Interfaces
{
    using System.Collections.Generic;

    using Common.Interfaces;

    public interface IMetadataLoader
    {
        IEnumerable<IModuleMetadata> LoadModuleMetadatas(IEnumerable<string> files);

        void RegisterMetadata(IModuleMetadata moduleMetadata);
    }
}