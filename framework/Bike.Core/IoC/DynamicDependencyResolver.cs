﻿namespace Bike.Core.IoC
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Common.Ioc;

    using Castle.Windsor;

    public class DynamicDependencyResolver : IDynamicDependencyResolver
    {
        public IWindsorContainer Container { get; set; }

        public bool CanResolve(Type type)
        {
            return Container.Kernel.HasComponent(type);
        }

        public IEnumerable<object> ResolveAllDependencies(Type type)
        {
            return Container.ResolveAll(type).Cast<object>();
        }

        public object ResolveDependency(Type type)
        {
            return Container.Resolve(type);
        }
    }
}