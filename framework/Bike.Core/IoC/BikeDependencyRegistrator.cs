﻿namespace Bike.Core.IoC
{
    using System;

    using Common.Ioc;

    using Castle.MicroKernel.Registration;
    using Castle.Windsor;

    public class BikeDependencyRegistrator : IBikeDependencyRegistrator
    {
        private readonly IWindsorContainer _container;

        public BikeDependencyRegistrator(IWindsorContainer container)
        {
            _container = container;
        }

        public IBikeDependencyRegistrator CreateChild()
        {
            var childContainer = new WindsorContainer();

            _container.AddChildContainer(childContainer);
            return new BikeDependencyRegistrator(childContainer);
        }

        public void Dispose()
        {
            _container.Dispose();
        }

        public void RegisterAsSingleton<TService, TImplementation>(string name = null) where TService : class where TImplementation : TService
        {
            _container.Register(Component.For<TService>().ImplementedBy<TImplementation>().LifestyleSingleton());
        }

        public void RegisterAsSingleton(Type serviceType, Type instanceType, string name = null)
        {
            _container.Register(Component.For(serviceType).ImplementedBy(instanceType).LifestyleSingleton());
        }

        public void RegisterAsSingleton<TService>(Type implmentationType, string name = null) where TService : class
        {
            _container.Register(Component.For<TService>().ImplementedBy(implmentationType).LifestyleSingleton());
        }

        public void RegisterAsTransient<TService, TImplementation>(string name = null) where TService : class where TImplementation : TService
        {
            _container.Register(Component.For<TService>().ImplementedBy<TImplementation>().LifestyleTransient());
        }

        public void RegisterAsTransient<TService>(Type implmentationType, string name = null) where TService : class
        {
            _container.Register(Component.For<TService>().ImplementedBy(implmentationType).LifestyleTransient());
        }

        public void RegisterInstance<TService>(object instance, string name = null) where TService : class
        {
            _container.Register(Component.For<TService>().Instance((TService)instance).Named(name));
        }

        public void RegisterPerThread<TService, TImplementation>(string name = null) where TService : class where TImplementation : TService
        {
            _container.Register(Component.For<TService>().ImplementedBy<TImplementation>().LifestylePerThread());
        }

        public void RegisterPerThread<TService>(Type implmentationType, string name = null) where TService : class
        {
            _container.Register(Component.For<TService>().ImplementedBy(implmentationType).LifestylePerThread());
        }

        public void RegisterPerThread(Type service, Type implmentationType, string name = null)
        {
            _container.Register(Component.For(service).ImplementedBy(implmentationType).LifestylePerThread());
        }

        public void RegisterPerThread(Type service, Func<object> factoryMethod, string name = null)
        {
            _container.Register(Component.For(service).UsingFactoryMethod(factoryMethod).LifestylePerThread());
        }

        public void Release(object instance)
        {
            _container.Release(instance);
        }
    }
}