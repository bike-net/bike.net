﻿namespace Bike.Core.IoC
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Common.Ioc;

    using Castle.Windsor;

    public class BikeDependencyResolver<TDependency> : IBikeDependencyResolver<TDependency>
    {
        public IWindsorContainer Container { get; set; }

        public IBikeResolverSession<TDependency> BeginSession()
        {
            return Container.Resolve<IBikeResolverSession<TDependency>>();
        }

        public void BeginSession(Action<IBikeResolverSession<TDependency>> action)
        {
            using (var session = BeginSession())
            {
                action(session);
            }
        }

        public async Task BeginSession(Func<IBikeResolverSession<TDependency>, Task> func)
        {
            using (var session = BeginSession())
            {
                await func(session);
            }
        }

        public bool CanResolve(Type type)
        {
            return Container.Kernel.GetAssignableHandlers(typeof(TDependency)).Any(h => h.ComponentModel.Implementation == type);
        }
    }

    public class BikeResolverSession<TDependency> : IBikeResolverSession<TDependency>
        where TDependency : class
    {
        private readonly List<TDependency> _list = new List<TDependency>();

        public IWindsorContainer Container { get; set; }

        public void Dispose()
        {
            Container.Release(this);

            foreach (var dependency in _list)
            {
                Container.Release(dependency);
            }

            _list.Clear();
        }

        public IEnumerable<TDependency> ResolveAllDependencies()
        {
            var depList = Container.ResolveAll<TDependency>();

            _list.AddRange(depList);

            return depList;
        }

        public TDependency ResolveDependency()
        {
            var dep = Container.Resolve<TDependency>();

            _list.Add(dep);

            return dep;
        }

        public TDependency ResolveDependency(Type type)
        {
            var dep = Container.Resolve<TDependency>(type.FullName);

            _list.Add(dep);

            return dep;
        }
    }
}