﻿namespace Bike.Core.Utils
{
    using System.Collections.Generic;
    using System.Linq;

    using NuGet.Versioning;

    public static class NugetUtils
    {
        public static VersionRange CombineVersionRange(this IEnumerable<VersionRange> versions)
        {
            NuGetVersion minVersion = null;
            var includeMinVersion = false;
            var minVersions = versions.Where(v => v.HasLowerBound)
                                      .ToArray();
            if (minVersions.Any())
            {
                var minVersionRange = minVersions.OrderByDescending(range => range, new MinVersionComparer())
                                                 .FirstOrDefault();
                if (minVersionRange != null)
                {
                    minVersion = minVersionRange.MinVersion;
                    includeMinVersion = minVersionRange.IsMinInclusive;
                }
            }

            NuGetVersion maxVersion = null;
            var includeMaxVersion = false;
            var maxVersions = versions.Where(v => v.HasUpperBound)
                                      .ToArray();
            if (maxVersions.Any())
            {
                var maxVersionRange = maxVersions.OrderBy(range => range, new MaxVersionComparer())
                                                 .FirstOrDefault();
                if (maxVersionRange != null)
                {
                    maxVersion = maxVersionRange.MaxVersion;
                    includeMaxVersion = maxVersionRange.IsMaxInclusive;
                }
            }

            return new VersionRange(minVersion, includeMinVersion, maxVersion, includeMaxVersion);
        }

        private class MaxVersionComparer : IComparer<VersionRange>
        {
            public int Compare(VersionRange x, VersionRange y)
            {
                var result = x.MinVersion.CompareTo(y.MinVersion);

                if (result != 0)
                {
                    return result;
                }

                if (x.IsMaxInclusive && !y.IsMaxInclusive)
                {
                    return 1;
                }

                if (!x.IsMaxInclusive && y.IsMaxInclusive)
                {
                    return -1;
                }

                return 0;
            }
        }

        private class MinVersionComparer : IComparer<VersionRange>
        {
            public int Compare(VersionRange x, VersionRange y)
            {
                var result = x.MinVersion.CompareTo(y.MinVersion);

                if (result != 0)
                {
                    return result;
                }

                if (x.IsMinInclusive && !y.IsMinInclusive)
                {
                    return -1;
                }

                if (!x.IsMinInclusive && y.IsMinInclusive)
                {
                    return 1;
                }

                return 0;
            }
        }
    }
}