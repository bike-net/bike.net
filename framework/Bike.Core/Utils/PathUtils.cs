﻿namespace Bike.Core.Utils
{
    using System.IO;
    using System.Reflection;

    public static class PathUtils
    {
        public static string GetExecutablePath()
        {
            var ass = Assembly.GetExecutingAssembly();
            return Path.GetDirectoryName(ass.Location);
        }
    }
}