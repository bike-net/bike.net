﻿namespace Bike.Core.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using Mono.Cecil;

    public static class CecilUtils
    {
        public static CustomAttribute GetCustomAttribute<T>(this TypeDefinition typeDefinition) where T : Attribute
        {
            return typeDefinition.GetCustomAttributes<T>()
                                 .FirstOrDefault();
        }

        public static IEnumerable<CustomAttribute> GetCustomAttributes<T>(this TypeDefinition typeDefinition) where T : Attribute
        {
            return typeDefinition.CustomAttributes.Where(ca => ca.AttributeType.FullName == typeof(T).FullName)
                                 .ToArray();
        }

        public static string GetDirectoryPathForType(this TypeDefinition typeDefinition, string excludeFolder)
        {
            return GetLeftPartOfPath(Path.GetDirectoryName(typeDefinition.GetFilePathForType()), excludeFolder);
        }

        public static string GetFilePathForType(this TypeDefinition typeDefinition)
        {
            var uri = new UriBuilder(typeDefinition.Module.Assembly.MainModule.FullyQualifiedName);
            return Uri.UnescapeDataString(uri.Path);
        }

        public static bool HasInterface<TInterface>(this TypeDefinition type)
        {
            return HasInterface(type, typeof(TInterface));
        }

        public static bool HasInterface(this TypeDefinition type, Type intefraceType)
        {
            return (type.Interfaces.Any(i => i.InterfaceType.Name == intefraceType.Name && i.InterfaceType.Namespace == intefraceType.Namespace) || type.NestedTypes.Any(t => HasInterface(t, intefraceType)));
        }

        #region Закрытые методы

        private static string GetLeftPartOfPath(string path, string endLeftPart)
        {
            // use the correct seperator for the environment
            var pathParts = path.Split(Path.DirectorySeparatorChar);

            // this assumes a case sensitive check. If you don't want this, you may want to loop through the pathParts looking
            // for your "startAfterPath" with a StringComparison.OrdinalIgnoreCase check instead
            var last = Array.IndexOf(pathParts, endLeftPart);

            if (last == -1)
            {
                // path path not found
                return null;
            }

            return string.Join(Path.DirectorySeparatorChar.ToString(CultureInfo.InvariantCulture), pathParts, 0, last);
        }

        #endregion
    }
}