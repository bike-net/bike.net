﻿namespace Bike.Core.Modules.TreeBuilders
{
    using System.Collections.Generic;

    using Interfaces;

    using Mono.Cecil;

    using QuickGraph;

    public class AssemblyGraph : AdjacencyGraph<AssembliesTreeVertex, Edge<AssembliesTreeVertex>>
    {
    }

    public class AssembliesTreeVertex
    {
        public AssemblyDefinition AssemblyDefinition { get; set; }

        public string AssemblyName { get; private set; }

        public AssembliesTreeVertex(string assemblyName)
        {
            AssemblyName = assemblyName;
        }
    }

    public class AssembliesTreeBuilder : IAssembliesTreeBuilder
    {
        private readonly Dictionary<string, AssembliesTreeVertex> _assembliesGraphVertex = new Dictionary<string, AssembliesTreeVertex>();

        private readonly AssemblyGraph _graph = new AssemblyGraph();

        public AssemblyGraph GetDependencyTree()
        {
            return _graph;
        }

        public AssembliesTreeVertex RegisterInGaph(AssemblyDefinition assemblyDefinition)
        {
            var assemblyName = assemblyDefinition.Name.Name;

            AssembliesTreeVertex targetVertex;
            if (!_assembliesGraphVertex.TryGetValue(assemblyName, out targetVertex))
            {
                _assembliesGraphVertex[assemblyName] = targetVertex = new AssembliesTreeVertex(assemblyName)
                                                                      {
                                                                          AssemblyDefinition = assemblyDefinition
                                                                      };
                _graph.AddVertex(targetVertex);
            }
            else
            {
                if (targetVertex.AssemblyDefinition == null)
                {
                    targetVertex.AssemblyDefinition = assemblyDefinition;
                }
                else if (targetVertex.AssemblyDefinition.Name.Version > assemblyDefinition.Name.Version)
                {
                    targetVertex.AssemblyDefinition = assemblyDefinition;
                }
            }

            return targetVertex;
        }

        public void RegisterInGaph(AssemblyDefinition assemblyDefinition, AssemblyNameReference assemblyReference)
        {
            var referenceName = assemblyReference.Name;

            var targetVertex = RegisterInGaph(assemblyDefinition);

            AssembliesTreeVertex sourceVertex;
            if (!_assembliesGraphVertex.TryGetValue(referenceName, out sourceVertex))
            {
                _assembliesGraphVertex[referenceName] = sourceVertex = new AssembliesTreeVertex(referenceName);

                _graph.AddVertex(sourceVertex);
            }

            if (!_graph.ContainsEdge(targetVertex, sourceVertex))
            {
                _graph.AddEdge(new Edge<AssembliesTreeVertex>(targetVertex, sourceVertex));
            }
        }
    }
}