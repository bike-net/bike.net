﻿namespace Bike.Core.Modules.TreeBuilders.Interfaces
{
    using Mono.Cecil;

    using QuickGraph;

    public interface IAssembliesTreeBuilder
    {
        AssemblyGraph GetDependencyTree();

        void RegisterInGaph(AssemblyDefinition assemblyDefinition, AssemblyNameReference assemblyReference);

        AssembliesTreeVertex RegisterInGaph(AssemblyDefinition assemblyDefinition);
    }
}