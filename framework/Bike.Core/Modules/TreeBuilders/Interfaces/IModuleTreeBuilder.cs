namespace Bike.Core.Modules.TreeBuilders.Interfaces
{
    using System.Threading.Tasks;

    using QuickGraph;

    public interface IModuleTreeBuilder
    {
        Task<AdjacencyGraph<ModuleTreeVertex, Edge<ModuleTreeVertex>>> BuildTree();
    }
}