﻿namespace Bike.Core.Modules.TreeBuilders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Common.Recipes;
    using Exceptions;
    using Resolvers.Interfaces;
    using Interfaces;

    using NuGet.Versioning;

    using QuickGraph;

    public class ModuleTreeVertex
    {
        public string ModuleName { get; private set; }

        public List<VersionRange> Versions { get; set; }

        public ModuleTreeVertex(string moduleName)
        {
            Versions = new List<VersionRange>();

            ModuleName = moduleName;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != GetType())
            {
                return false;
            }
            return Equals((ModuleTreeVertex)obj);
        }

        public override int GetHashCode()
        {
            return (ModuleName != null
                        ? ModuleName.GetHashCode()
                        : 0);
        }

        private bool Equals(ModuleTreeVertex other)
        {
            return string.Equals(ModuleName, other.ModuleName);
        }
    }

    public class ModuleTreeBuilder : IModuleTreeBuilder
    {
        private readonly AdjacencyGraph<ModuleTreeVertex, Edge<ModuleTreeVertex>> _modulesGraph = new AdjacencyGraph<ModuleTreeVertex, Edge<ModuleTreeVertex>>();

        public IRecipe Recipe { get; set; }

        public ICommonModuleResolver ModuleResolver { get; set; }

        public async Task<AdjacencyGraph<ModuleTreeVertex, Edge<ModuleTreeVertex>>> BuildTree()
        {
            foreach (var dependencyItem in Recipe.GetDependencies())
            {
                var version = dependencyItem.Version != null
                                  ? VersionRange.Parse(dependencyItem.Version)
                                  : VersionRange.All;
                await RegisterModuleOnTree(dependencyItem.ModuleName, version);
            }

            foreach (var implimentationOfContract in Recipe.GetImplimentationOfContracts())
            {
                if (_modulesGraph.Vertices.Any(vertex => vertex.ModuleName == implimentationOfContract.ContractName))
                {
                    await RegisterModuleOnTree(implimentationOfContract.ModuleName, VersionRange.All);
                }
            }

            return _modulesGraph;
        }

        #region Закрытые методы

       

        private async Task<ModuleTreeVertex> RegisterModuleOnTree(string moduleName, VersionRange version)
        {
            var moduleMetadata = await ModuleResolver.ResolveModule(moduleName, version);
            if (moduleMetadata == null)
            {
                throw new MissingModuleException(moduleName, version.ToString());
            }

            var moduleVertex = _modulesGraph.Vertices.FirstOrDefault(v => v.ModuleName.Equals(moduleName, StringComparison.InvariantCultureIgnoreCase));
            if (moduleVertex == null)
            {
                moduleVertex = new ModuleTreeVertex(moduleName);
                _modulesGraph.AddVertex(moduleVertex);
            }

            if (!moduleVertex.Versions.Any(v => v.Equals(version)))
            {
                moduleVertex.Versions.Add(version);

                foreach (var moduleDependency in moduleMetadata.ImportModules.Union(moduleMetadata.ImportContracts))
                {
                    var depVertex = await RegisterModuleOnTree(moduleDependency.Name, moduleDependency.Version);
                    _modulesGraph.AddEdge(new Edge<ModuleTreeVertex>(moduleVertex, depVertex));
                }

                if (moduleMetadata.ExportContract != null)
                {
                    var depVertex = await RegisterModuleOnTree(moduleMetadata.ExportContract.Name, moduleMetadata.ExportContract.Version);
                    _modulesGraph.AddEdge(new Edge<ModuleTreeVertex>(depVertex, moduleVertex));
                    
                }
            }

            return moduleVertex;
        }

        #endregion
    }
}