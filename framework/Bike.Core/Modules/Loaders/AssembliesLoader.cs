﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Bike.Core.Modules.Loaders.Interfaces;
using Bike.Core.Modules.TreeBuilders;
using Bike.Core.Modules.TreeBuilders.Interfaces;
using Bike.Core.Providers.Interfaces;
using Bike.Core.Utils;
using Mono.Cecil;

namespace Bike.Core.Modules.Loaders
{
    public class AssembliesLoader : IAssembliesLoader
    {
        private const string DllSearchPattern = "*.dll";

        public IModularRecipeProvider RecipeProvider { get; set; }

        public IAssembliesTreeBuilder AssembliesTreeBuilder { get; set; }

        private string ModuleBinDirectory
        {
            get
            {
                var directory = Path.Combine(PathUtils.GetExecutablePath(), RecipeProvider.ModulesFolderBin);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                return directory;
            }
        }

        public AssemblyGraph GetAssembliesTree()
        {
            var files = RecipeProvider.ModulesPaths.SelectMany(p => GetFiles(p, RecipeProvider.ModulesSearchPattern));

            foreach (var file in files)
            {
                var assemblyDefinition = AssemblyDefinition.ReadAssembly(file);

                AssembliesTreeBuilder.RegisterInGaph(assemblyDefinition);

                foreach (var moduleDefinition in assemblyDefinition.Modules)
                {
                    AddAssemblyReferencesToGraph(assemblyDefinition, moduleDefinition);
                }
            }

            var tree = AssembliesTreeBuilder.GetDependencyTree();
            return tree;
        }

        public IEnumerable<Assembly> LoadAssemblies(IEnumerable<string> assembliesNames, AssemblyGraph tree)
        {
            ClearModulesBin();
            return
                assembliesNames.Select(assemblyName => CopyAssemblyWithReference(tree, assemblyName))
                    .Select(Assembly.LoadFrom);
        }

        private string CopyAssemblyWithReference(AssemblyGraph tree, string assemblyName)
        {
            var assemblyVertex = tree.Vertices.First(v => v.AssemblyName == assemblyName);
            var edges = tree.OutEdges(assemblyVertex);

            foreach (var edge in edges.Where(e => e.Target.AssemblyDefinition != null))
            {
                CopyAssemblyWithReference(tree, edge.Target.AssemblyName);
            }

            var assemblyPath = assemblyVertex.AssemblyDefinition.MainModule.FullyQualifiedName;
            return CopyToModulesBin(assemblyPath);
        }

        #region Закрытые методы

        private void AddAssemblyReferencesToGraph(AssemblyDefinition assemblyDefinition,
            ModuleDefinition moduleDefinition)
        {
            if (!moduleDefinition.HasAssemblyReferences)
            {
                return;
            }

            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (
                var assemblyReference in
                    moduleDefinition.AssemblyReferences.Where(
                        a => loadedAssemblies.All(la => la.GetName().Name != a.Name)))
            {
                AssembliesTreeBuilder.RegisterInGaph(assemblyDefinition, assemblyReference);
            }
        }

        private void ClearModulesBin()
        {
            var info = new DirectoryInfo(ModuleBinDirectory);
            foreach (var file in info.GetFiles())
            {
                file.Delete();
            }
            foreach (var subDirectory in info.GetDirectories())
            {
                subDirectory.Delete(true);
            }
        }

        private string CopyToModulesBin(string assemblyPath)
        {
            var newFile = Path.Combine(ModuleBinDirectory, Path.GetFileName(assemblyPath));

            if (!File.Exists(newFile))
            {
                File.Copy(assemblyPath, newFile, true);
            }

            return newFile;
        }

        private IEnumerable<string> FilterFiles(IEnumerable<string> files, string searchPattern)
        {
            var regex = new Regex(searchPattern);

            return files.Where(f => regex.IsMatch(f));
        }

        private IEnumerable<string> GetFiles(string modulesPath, string searchPattern)
        {
            var allFiles = Directory.GetFiles(modulesPath, DllSearchPattern, SearchOption.AllDirectories);

            return FilterFiles(allFiles, searchPattern);
        }

        #endregion
    }
}