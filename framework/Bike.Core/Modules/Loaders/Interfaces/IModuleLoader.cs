﻿namespace Bike.Core.Modules.Loaders.Interfaces
{
    using System.Threading.Tasks;

    public interface IModuleLoader
    {
        Task LoadModules();
    }
}