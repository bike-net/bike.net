namespace Bike.Core.Modules.Loaders.Interfaces
{
    using System.Collections.Generic;
    using System.Reflection;

    using TreeBuilders;

    public interface IAssembliesLoader
    {
        AssemblyGraph GetAssembliesTree();

        IEnumerable<Assembly> LoadAssemblies(IEnumerable<string> assembliesNames , AssemblyGraph tree);
    }
}