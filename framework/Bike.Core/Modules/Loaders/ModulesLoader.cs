﻿namespace Bike.Core.Modules.Loaders
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Common.Interfaces;
    using Common.Ioc;
    using Metadata.Loaders.Interfaces;
    using Interfaces;
    using Resolvers.Interfaces;
    using TreeBuilders.Interfaces;
    using Utils;

    using QuickGraph.Algorithms;

    public class ModulesLoader : IModuleLoader
    {
        public ICommonModuleResolver ModuleResolver { get; set; }

        public IModuleTreeBuilder ModuleTreeBuilder { get; set; }

        public IMetadataLoader MetadataLoader { get; set; }

        public IAssembliesLoader AssembliesLoader { get; set; }

        public IBikeDependencyRegistrator BikeDependencyRegistrator { get; set; }

        public IBikeDependencyResolver<IBikeModule> ModuleDependencyResolver { get; set; }

        public async Task LoadModules()
        {
            var metadatas = await RegisterMetadatas();

            RegisterModules(metadatas);
        }

        #region Закрытые методы

        private async Task<IEnumerable<IModuleMetadata>> RegisterMetadatas()
        {
            var moduleTree = await ModuleTreeBuilder.BuildTree();

            var metadatas = new List<IModuleMetadata>();
            foreach (var moduleVertex in moduleTree.TopologicalSort())
            {
                var version = moduleVertex.Versions.CombineVersionRange();

                var moduleMetadata = await ModuleResolver.ResolveModule(moduleVertex.ModuleName, version);

                MetadataLoader.RegisterMetadata(moduleMetadata);

                metadatas.Add(moduleMetadata);
            }

            return metadatas;
        }

        private void RegisterModules(IEnumerable<IModuleMetadata> metadatas)
        {
            var assemblyTree = AssembliesLoader.GetAssembliesTree();
            var moduleAssemblies = AssembliesLoader.LoadAssemblies(metadatas.Select(m => m.AssemblyName), assemblyTree);

            foreach (var assembly in moduleAssemblies)
            {
                BikeDependencyRegistrator.RegisterAsTransient<IBikeModule>(assembly.GetTypes().Single(t => typeof(IBikeModule).IsAssignableFrom(t)));
            }
            ModuleDependencyResolver.BeginSession(
                session =>
                {
                    var modules = session.ResolveAllDependencies();
                    foreach (var bikeModule in modules)
                    {
                        bikeModule.RegisterDependency();
                    }

                    foreach (var bikeModule in modules)
                    {
                        bikeModule.Initializing();
                    }
                });
        }

        #endregion
    }
}