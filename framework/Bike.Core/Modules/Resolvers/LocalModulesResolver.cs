﻿namespace Bike.Core.Modules.Resolvers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Common.Interfaces;
    using Metadata.Loaders.Interfaces;
    using Loaders.Interfaces;
    using Interfaces;

    using NuGet.Versioning;

    using QuickGraph.Algorithms;

    public class LocalModulesResolver : ILocalModuleResolver
    {
        private readonly Lazy<IEnumerable<IModuleMetadata>> _metadatas;

        public IAssembliesLoader AssemblyLoader { get; set; }

        public IMetadataLoader MetadataLoader { get; set; }

        public LocalModulesResolver()
        {
            _metadatas = new Lazy<IEnumerable<IModuleMetadata>>(
                () =>
                {
                    var tree = AssemblyLoader.GetAssembliesTree();

                    var allVertex = tree.TopologicalSort().Where(vertex => vertex.AssemblyDefinition != null);

                    var files = allVertex.Select(v => v.AssemblyDefinition.MainModule.FullyQualifiedName);

                    return MetadataLoader.LoadModuleMetadatas(files);
                });
        }

        public Task<IModuleMetadata> GetModuleMetadata(string moduleName, VersionRange versionRange)
        {
            var moduleMetadata = GetModuleByVersion(moduleName, versionRange).OrderBy(m => m.Version).FirstOrDefault();

            return Task.FromResult(moduleMetadata);
        }

        public Task<bool> IsExist(string moduleName, VersionRange versionRange)
        {
            var exist = GetModuleByVersion(moduleName, versionRange).Any();

            return Task.FromResult(exist);
        }

        #region Закрытые методы

        private IEnumerable<IModuleMetadata> GetModuleByVersion(string moduleName, VersionRange versionRange)
        {
            return _metadatas.Value.Where(m => m.FullName.Equals(moduleName, StringComparison.InvariantCultureIgnoreCase) && versionRange.Satisfies(m.Version));
        }

        #endregion
    }
}