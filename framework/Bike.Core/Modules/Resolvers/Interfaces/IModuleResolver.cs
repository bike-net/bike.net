namespace Bike.Core.Modules.Resolvers.Interfaces
{
    using System.Threading.Tasks;

    using Common.Interfaces;

    using NuGet.Versioning;

    public interface IModuleResolver
    {
        Task<IModuleMetadata> GetModuleMetadata(string moduleName, VersionRange versionRange);

        Task<bool> IsExist(string moduleName, VersionRange versionRange);
    }
}