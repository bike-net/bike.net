﻿namespace Bike.Core.Modules.Resolvers.Interfaces
{
    using System.Threading.Tasks;

    using Common.Interfaces;

    using NuGet.Versioning;

    public interface ICommonModuleResolver
    {
        Task<IModuleMetadata> ResolveModule(string moduleName, VersionRange version);
    }
}