﻿using System.Threading.Tasks;

namespace Bike.Core.Modules.Resolvers
{
    using System;

    using Common.Interfaces;
    using Interfaces;

    using NuGet.Versioning;

    public class CommonModuleResolver : ICommonModuleResolver
    {
        public IStoreModuleResolver StoreModuleResolver { get; set; }

        public ILocalModuleResolver LocalModuleResolver { get; set; }

        public async Task<IModuleMetadata> ResolveModule(string moduleName, VersionRange version)
        {
            var moduleLoader = await CheckModuleLoader(LocalModuleResolver, moduleName, version); //?? await CheckModuleLoader(StoreModuleResolver, moduleName, version);

            if (moduleLoader == null)
            {
                throw new Exception(string.Format("Невозможно найти модуль '{0}' с версией {1}", moduleName, version.ToNormalizedString()));
            }

            return await moduleLoader.GetModuleMetadata(moduleName, version);
        }

        private async Task<IModuleResolver> CheckModuleLoader(IModuleResolver moduleResolver, string moduleName, VersionRange version)
        {
            return await moduleResolver.IsExist(moduleName, version)
                       ? moduleResolver
                       : null;
        }
    }
}
