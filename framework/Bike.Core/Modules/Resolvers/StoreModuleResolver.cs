﻿namespace Bike.Core.Modules.Resolvers
{
    using System;
    using System.Threading.Tasks;

    using Common.Interfaces;
    using Interfaces;

    using NuGet.Versioning;

    public class StoreModuleResolver : IStoreModuleResolver
    {
        public Task<IModuleMetadata> GetModuleMetadata(string moduleName, VersionRange versionRange)
        {
            throw new NotImplementedException();
        }

        public void Init()
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsExist(string moduleName, VersionRange versionRange)
        {
            throw new NotImplementedException();
        }
    }
}