﻿namespace Bike.Core.Exceptions
{
    using Common.Exceptions;

    public class MissingModuleException : BikeException
    {
        private readonly string _moduleName;

        private readonly string _version;

        /// <summary>Gets a message that describes the current exception.</summary>
        /// <returns>The error message that explains the reason for the exception, or an empty string("").</returns>
        public override string Message
        {
            get { return string.Format("Невозможно найти модуль {0} с версией {1}", _moduleName, _version); }
        }

        public MissingModuleException(string moduleName, string version)
        {
            _moduleName = moduleName;
            _version = version;
        }

        public string GetModuleName()
        {
            return _moduleName;
        }

        public string GetVersion()
        {
            return _version;
        }
    }
}