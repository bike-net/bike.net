﻿namespace Bike.Core.Exceptions
{
    using BarsGroup.CodeGuard;

    using Common.Exceptions;

    public class DuplicateModuleException : BikeException
    {
        private readonly string _moduleName;

        private readonly string _path;

        public override string Message
        {
            get { return string.Format("Модуль {0}, находящийся по пути '{1}', уже зарегистрирован", _moduleName, _path); }
        }

        public DuplicateModuleException(string moduleName, string path)
        {
            Guard.That(moduleName)
                 .IsNotNullOrWhiteSpace();
            Guard.That(path)
                 .IsNotNullOrWhiteSpace();

            _moduleName = moduleName;
            _path = path;
        }
    }
}