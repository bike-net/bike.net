﻿namespace Bike.Core.Exceptions
{
    using Common.Exceptions;

    public class ContractResolveMismatchException : BikeException
    {
        public ContractResolveMismatchException(string message) : base(message)
        {
        }
    }
}