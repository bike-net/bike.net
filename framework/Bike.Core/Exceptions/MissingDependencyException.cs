﻿namespace Bike.Core.Exceptions
{
    using BarsGroup.CodeGuard;

    using Common.Exceptions;

    public class MissingDependencyException : BikeException
    {
        private readonly string _source;

        private readonly string _target;

        public override string Message
        {
            get { return string.Format("Отсутствует зависимость модуля {0} от модуля {1}", _source, _target); }
        }

        public MissingDependencyException(string source, string target)
        {
            Guard.That(_source)
                 .IsNotNullOrEmpty();
            Guard.That(target)
                 .IsNotNullOrEmpty();

            _source = source;
            _target = target;
        }
    }
}