﻿namespace Bike.Core
{
    using System;
    using System.Threading.Tasks;

    using Common.Ioc;
    using Common.Recipes;
    using IoC;
    using Metadata.Loaders;
    using Metadata.Loaders.Interfaces;
    using Modules.Loaders;
    using Modules.Loaders.Interfaces;
    using Modules.Resolvers;
    using Modules.Resolvers.Interfaces;
    using Modules.TreeBuilders;
    using Modules.TreeBuilders.Interfaces;
    using Providers;
    using Providers.Interfaces;
    using Recipes;

    using Castle.Facilities.TypedFactory;
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.Resolvers;
    using Castle.MicroKernel.Resolvers.SpecializedResolvers;
    using Castle.Windsor;

    public class BikeApplication
    {
        public const string BikeCoreName = "core";


        private IBikeDependencyRegistrator _registrator;

        public static async Task Run(Action<IBikeDependencyRegistrator> registerDependency)
        {
            await new BikeApplication().RunPrivate(registerDependency);
        }

        #region Закрытые методы

        private IWindsorContainer ConfigureContainer()
        {
            var container = InitContainer();
            _registrator = container.Resolve<IBikeDependencyRegistrator>();

            return container;
        }

        private IWindsorContainer InitContainer()
        {
            var winsdorContainer = new WindsorContainer();
            winsdorContainer.AddFacility<TypedFactoryFacility>();
            winsdorContainer.Kernel.Resolver.AddSubResolver(new CollectionResolver(winsdorContainer.Kernel));
            winsdorContainer.Register(
                Component.For<ILazyComponentLoader>()
                         .ImplementedBy<LazyOfTComponentLoader>(),
                Component.For<IWindsorContainer>()
                         .Instance(winsdorContainer),
                Component.For<IBikeDependencyRegistrator>()
                         .ImplementedBy<BikeDependencyRegistrator>());
            return winsdorContainer;
        }

        private async Task LoadModules(IBikeDependencyResolver<IModuleLoader> moduleLoaderResolver)
        {
            await moduleLoaderResolver.BeginSession(
                async session =>
                {
                    var modulesLoader = session.ResolveDependency();
                    await modulesLoader.LoadModules();
                });
        }

        private void LoadRecipe()
        {
            _registrator.RegisterInstance<IRecipe>(Recipe.LoadRecipe());
        }

        private void RegisterDependency()
        {
            _registrator.RegisterAsSingleton<ICommonModuleResolver, CommonModuleResolver>();
            _registrator.RegisterAsSingleton<IMetadataLoader, MetadataLoader>();
            _registrator.RegisterAsSingleton<IModularRecipeProvider, CoreRecipeProvider>();
            _registrator.RegisterAsSingleton<IAssembliesTreeBuilder, AssembliesTreeBuilder>();
            _registrator.RegisterAsSingleton<IAssembliesLoader, AssembliesLoader>();
            _registrator.RegisterAsSingleton<ILocalModuleResolver, LocalModulesResolver>();
            _registrator.RegisterAsTransient<IModuleLoader, ModulesLoader>();
            _registrator.RegisterAsSingleton<IModuleTreeBuilder, ModuleTreeBuilder>();
            _registrator.RegisterAsSingleton<IDynamicDependencyResolver, DynamicDependencyResolver>();
            _registrator.RegisterAsSingleton(typeof(IBikeDependencyResolver<>), typeof(BikeDependencyResolver<>));
            _registrator.RegisterAsSingleton(typeof(IBikeResolverSession<>), typeof(BikeResolverSession<>));

        }

        private async Task<IWindsorContainer> RunPrivate(Action<IBikeDependencyRegistrator> registerExternalDependency)
        {
            var container = ConfigureContainer();

            //Запускаем регистрацию внешниъ зависимостей
            if (registerExternalDependency != null)
            {
                registerExternalDependency(_registrator);
            }


            RegisterDependency();
            SetupEnvironment();
            LoadRecipe();
            await LoadModules(container.Resolve<IBikeDependencyResolver<IModuleLoader>>());


            return container;
        }

        private void SetupEnvironment()
        {
            Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
        }

        #endregion
    }
}