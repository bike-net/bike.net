﻿namespace Bike.Core.Recipes
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Common.Recipes;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class Recipe : IRecipe
    {
        private const string SettingsName = "settings";

        private const string DependenciesName = "dependencies";

        private const string ImplimentationOfContractsName = "contracts";

        private const string RecipeFileName = "bike.json";

        private JObject _jRecipe = new JObject();

        public IEnumerable<IRecipeDependencyItem> GetDependencies()
        {
            JToken dependecies = _jRecipe[DependenciesName];
            if (dependecies == null)
            {
                dependecies = _jRecipe[DependenciesName] = new JArray();
                SaveRecipe(this);
            }
            
            return dependecies.ToObject<IEnumerable<RecipeDependencyItem>>();
        }

        public IEnumerable<IRecipeImplementationOfContractItem> GetImplimentationOfContracts()
        {
            JToken implOfContracts = _jRecipe[ImplimentationOfContractsName];
            if (implOfContracts == null)
            {
                implOfContracts = _jRecipe[ImplimentationOfContractsName] = new JArray();
                SaveRecipe(this);
            }

            return implOfContracts.ToObject<IEnumerable<IRecipeImplementationOfContractItem>>();
        }

        public T GetRequiredSettings<T>(string name, string moduleName)
        {
            var value = GetSettings(name, moduleName, default(T));

            if (Equals(value, default(T)))
            {
                throw new ArgumentException(string.Format("В конфигурации не определена обязательнаая настройка {0}", name));
            }

            return value;
        }

        public T GetSettings<T>(string name, string moduleName, T defaultValue)
        {
            ChangeRegisterLetterModuleName(ref moduleName);
            ChangeRegisterLetterModuleName(ref name);

            if (_jRecipe[SettingsName] == null || _jRecipe[SettingsName][moduleName] == null || _jRecipe[SettingsName][moduleName][name] == null)
            {
                SetSettings(name, moduleName, defaultValue);
                return defaultValue;
            }

            return _jRecipe[SettingsName][moduleName][name].ToObject<T>();
        }

        public void Load(Stream stream)
        {
            _jRecipe = JObject.Load(new JsonTextReader(new StreamReader(stream)));
        }

        public static IRecipe LoadRecipe()
        {
            var file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, RecipeFileName);

            var recipe = new Recipe();

            if (File.Exists(file))
            {
                using (Stream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
                {
                    recipe.Load(stream);
                }
            }

            return recipe;
        }

        public void Save(Stream stream)
        {
            var sw = new StreamWriter(stream);
            sw.Write(_jRecipe.ToString());
            sw.Flush();
        }

        public static void SaveRecipe(IRecipe recipe)
        {
            var file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, RecipeFileName);

            if (File.Exists(file))
            {
                File.Delete(file);
            }

            using (Stream stream = new FileStream(file, FileMode.CreateNew, FileAccess.Write))
            {
                recipe.Save(stream);
            }
        }

        public void SetSettings<T>(string name, string moduleName, T value)
        {
            ChangeRegisterLetterModuleName(ref moduleName);
            ChangeRegisterLetterModuleName(ref name);

            if (_jRecipe[SettingsName] == null)
            {
                _jRecipe.Add(SettingsName, new JObject());
            }

            if (_jRecipe[SettingsName][moduleName] == null)
            {
                ((JObject)_jRecipe[SettingsName]).Add(moduleName, new JObject());
            }

            if (_jRecipe[SettingsName][moduleName][name] == null)
            {
                ((JObject)_jRecipe[SettingsName][moduleName]).Add(name, new JValue(default(T)));
            }

            var jValue = !Equals(value, null)
                                 ? JToken.FromObject(value)
                                 : JToken.FromObject(string.Empty);

            _jRecipe[SettingsName][moduleName][name] = jValue;

            SaveRecipe(this);
        }

        private static void ChangeRegisterLetterModuleName(ref string moduleName)
        {
            moduleName = char.ToLowerInvariant(moduleName[0]) + moduleName.Substring(1);
        }
    }
}