﻿namespace Bike.Core.Recipes
{
    using Common.Recipes;

    public class RecipeDependencyItem : IRecipeDependencyItem
    {
        public string ModuleName { get; set; }

        public string Version { get; set; }
    }
}