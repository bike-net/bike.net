﻿namespace Bike.Core.Recipes
{
    using Common.Recipes;

    public class RecipeImplementationOfContractItem : IRecipeImplementationOfContractItem
    {
        public string ContractName { get; set; }

        public string ModuleName { get; set; }
    }
}