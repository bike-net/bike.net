﻿namespace Bike.Common.Recipes
{
    using Newtonsoft.Json;

    [JsonObject]
    public interface IRecipeDependencyItem
    {
        [JsonProperty("ModuleName")]
        string ModuleName { get; set; }

        [JsonProperty("Version")]
        string Version { get; set; }
    }
}