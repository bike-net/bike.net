namespace Bike.Common.Recipes
{
    using Newtonsoft.Json;

    [JsonObject]
    public interface IRecipeImplementationOfContractItem
    {
        [JsonProperty("ContractName")]
        string ContractName { get; set; }

        [JsonProperty("ModuleName")]
        string ModuleName { get; set; }
    }
}