﻿namespace Bike.Common.Recipes
{
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// Настройки платформы
    /// </summary>
    public interface IRecipe
    {
        /// <summary>
        /// Получение сприска зависимостей
        /// </summary>
        /// <returns>Список зависимостей</returns>
        IEnumerable<IRecipeDependencyItem> GetDependencies();

        /// <summary>
        /// Получение сопоставления контрактов
        /// </summary>
        /// <returns>Список сопоставление контрактов</returns>
        IEnumerable<IRecipeImplementationOfContractItem> GetImplimentationOfContracts();

        /// <summary>
        /// Получение значения настройки
        /// </summary>
        /// <typeparam name="T">Тип значения</typeparam>
        /// <param name="name">Ключ</param>
        /// <param name="moduleName">Название модуля</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <returns>Значение настройки</returns>
        T GetSettings<T>(string name, string moduleName, T defaultValue);

        /// <summary>
        /// Получение значения настройки. Если настройка не заполнена - бросается исключение
        /// </summary>
        /// <typeparam name="T">Тип значения</typeparam>
        /// <param name="name">Ключ</param>
        /// <param name="moduleName">Название модуля</param>
        /// <returns>Значение настройки</returns>
        T GetRequiredSettings<T>(string name, string moduleName);

        /// <summary>
        /// Загрузить настройки с потока
        /// </summary>
        /// <param name="stream">Поток</param>
        void Load(Stream stream);

        /// <summary>
        /// Сохранеить настройки в поток
        /// </summary>
        /// <param name="stream">Поток</param>
        void Save(Stream stream);

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        /// <typeparam name="T">Тип значения</typeparam>
        /// <param name="name">Ключ</param>
        /// <param name="moduleName">Название модуля</param>
        /// <param name="value">Значение настройки</param>
        void SetSettings<T>(string name, string moduleName, T value);
    }
}