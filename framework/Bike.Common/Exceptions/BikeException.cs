﻿namespace Bike.Common.Exceptions
{
    using System;

    public abstract class BikeException : Exception
    {
        protected BikeException()
        {
            
        }
        protected BikeException(string message) : base(message)
        {
        }
    }
}