﻿namespace Bike.Common.Providers
{
    using Recipes;

    public abstract class BaseRecipeProvider
    {
        public IRecipe Resipe { get; set; }
    }
}