﻿namespace Bike.Common.Ioc
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IBikeDependencyResolver<out TDependency>
    {
        IBikeResolverSession<TDependency> BeginSession();

        bool CanResolve(Type type);

        void BeginSession(Action<IBikeResolverSession<TDependency>> action);

        Task BeginSession(Func<IBikeResolverSession<TDependency>, Task> func);
    }

    public interface IBikeResolverSession<out TDependency> : IDisposable
    {
        IEnumerable<TDependency> ResolveAllDependencies();

        TDependency ResolveDependency();

        TDependency ResolveDependency(Type type);
    }
}