﻿namespace Bike.Common.Ioc
{
    using System;
    using System.Collections.Generic;

    public interface IDynamicDependencyResolver
    {
        bool CanResolve(Type type);

        IEnumerable<object> ResolveAllDependencies(Type type);

        object ResolveDependency(Type type);
    }
}