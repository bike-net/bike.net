﻿namespace Bike.Common.Ioc
{
    using System;

    /// <summary>Регистратор зависимостей</summary>
    public interface IBikeDependencyRegistrator : IDisposable
    {
        /// <summary>Создание дочернего регистратора</summary>
        /// <returns>Дочерний регистратор</returns>
        IBikeDependencyRegistrator CreateChild();

        /// <summary>Зарегистрировать зависимость как одиночку</summary>
        /// <typeparam name="TService">Тип сервиса</typeparam>
        /// <typeparam name="TImplementation">Тип реализации</typeparam>
        /// <param name="name">Название зависимости</param>
        void RegisterAsSingleton<TService, TImplementation>(string name = null) where TService : class where TImplementation : TService;

        /// <summary>Зарегистрировать зависимость как одиночку</summary>
        /// <param name="serviceType">Тип сервиса</param>
        /// <param name="instanceType">Тип реализации</param>
        /// <param name="name">Название зависимости</param>
        void RegisterAsSingleton(Type serviceType, Type instanceType, string name = null);

        /// <summary>Зарегистрировать зависимость как одиночку</summary>
        /// <typeparam name="TService">Тип сервиса</typeparam>
        /// <param name="implmentationType">Тип реализации</param>
        /// <param name="name">Название зависимости</param>
        void RegisterAsSingleton<TService>(Type implmentationType, string name = null) where TService : class;

        /// <summary>Зарегистрировать зависимость как временную</summary>
        /// <typeparam name="TService">Тип сервиса</typeparam>
        /// <typeparam name="TImplementation">Тип реализации</typeparam>
        /// <param name="name">Название зависимости</param>
        void RegisterAsTransient<TService, TImplementation>(string name = null) where TService : class where TImplementation : TService;

        /// <summary>Зарегистрировать зависимость как одиночку</summary>
        /// <typeparam name="TService">Тип сервиса</typeparam>
        /// <param name="implmentationType">Тип реализации</param>
        /// <param name="name">Название зависимости</param>
        void RegisterAsTransient<TService>(Type implmentationType, string name = null) where TService : class;

        /// <summary>Зарегистрировать зависимость через экземпляра</summary>
        /// <typeparam name="TService">Тип сервиса</typeparam>
        /// <param name="instance">Объект реализации</param>
        /// <param name="name">Название зависимости</param>
        void RegisterInstance<TService>(object instance, string name = null) where TService : class;

        /// <summary>Зарегистрировать зависимость на поток</summary>
        /// <typeparam name="TService">Тип сервиса</typeparam>
        /// <typeparam name="TImplementation">Тип реализации</typeparam>
        /// <param name="name">Название зависимости</param>
        void RegisterPerThread<TService, TImplementation>(string name = null) where TService : class where TImplementation : TService;

        /// <summary>Зарегистрировать зависимость на поток</summary>
        /// <typeparam name="TService">Тип сервиса</typeparam>
        /// <param name="implmentationType">Тип реализации</param>
        /// <param name="name">Название зависимости</param>
        void RegisterPerThread<TService>(Type implmentationType, string name = null) where TService : class;

        /// <summary>Зарегистрировать зависимость на поток</summary>
        /// <param name="service">Тип сервиса</param>
        /// <param name="implmentationType">Тип реализации</param>
        /// <param name="name">Название зависимости</param>
        void RegisterPerThread(Type service, Type implmentationType, string name = null);

        /// <summary>Зарегистрировать зависимость на поток</summary>
        /// <param name="service">Тип сервиса</param>
        /// <param name="factoryMethod">Фабричный метод</param>
        /// <param name="name">Название зависимости</param>
        void RegisterPerThread(Type service, Func<object> factoryMethod, string name = null);

        /// <summary>Освободить объект</summary>
        /// <param name="instance">Экземпляр зависимости</param>
        void Release(object instance);
    }
}