﻿namespace Bike.Common.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class BikeImportModuleAttribute : Attribute
    {
        private readonly string _moduleName;

        private readonly string _version;

        public BikeImportModuleAttribute(string moduleName, string version)
        {
            _moduleName = moduleName;
            _version = version;
        }

        public string GetModuleName()
        {
            return _moduleName;
        }

        public string GetVersion()
        {
            return _version;
        }
    }
}