﻿namespace Bike.Common.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class BikeExportContractAttribute : Attribute
    {
        private readonly string _contractName;

        private readonly string _version;

        public string ContractName
        {
            get { return _contractName; }
        }

        public string Version
        {
            get { return _version; }
        }

        public BikeExportContractAttribute(string contractName, string version)
        {
            _contractName = contractName;
            _version = version;
        }
    }
}