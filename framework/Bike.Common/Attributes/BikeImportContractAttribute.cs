﻿namespace Bike.Common.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class BikeImportContractAttribute : Attribute
    {
        private readonly string _contractName;

        private readonly string _version;

        public BikeImportContractAttribute(string contractName, string version)
        {
            _contractName = contractName;
            _version = version;
        }

        public string GetContractName()
        {
            return _contractName;
        }

        public string GetVersion()
        {
            return _version;
        }
    }
}