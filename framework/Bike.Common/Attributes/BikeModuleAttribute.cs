﻿namespace Bike.Common.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class BikeModuleAttribute : Attribute
    {
        private readonly string _name;

        private readonly string _version;

        public BikeModuleAttribute(string name, string version)
        {
            _name = name;
            _version = version;
        }

        public string GetName()
        {
            return _name;
        }

        public string GetVersion()
        {
            return _version;
        }
    }
}