﻿namespace Bike.Common.Interfaces
{
    using System.Collections.Generic;

    using NuGet.Versioning;

    public interface IModuleIdentity
    {
        /// <summary>Название модуля</summary>
        string FullName { get; }

        /// <summary>Версия модуля</summary>
        NuGetVersion Version { get; }
    }

    public interface IModuleDependency
    {
        /// <summary>Название модуля</summary>
        string Name { get; }

        /// <summary>Версия модуля</summary>
        VersionRange Version { get; }
    }

    public interface IModuleMetadata : IModuleIdentity
    {
        /// <summary>Короткое название модуля</summary>
        string ShortName { get; }

        /// <summary>Простарнство имен типа модуля</summary>
        string ModuleTypeNamespace { get; }

        /// <summary>Название сборки</summary>
        string AssemblyName { get; }

        /// <summary>Полный путь до папки модуля</summary>
        string FolderPath { get; }

        /// <summary>Название модулей от которых завист модуль</summary>
        IEnumerable<IModuleDependency> ImportModules { get; }

        /// <summary>Типы контрактов которые требуются модулю</summary>
        IEnumerable<IModuleDependency> ImportContracts { get; }

        /// <summary>Тип контракта который реализует модуль</summary>
        IModuleDependency ExportContract { get; }

        /// <summary>Является контрактом</summary>
        bool IsContract { get; }
    }
}