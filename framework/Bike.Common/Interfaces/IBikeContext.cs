﻿namespace Bike.Common.Interfaces
{
    public interface IBikeContext
    {
        string ModulesVirtualPath { get; }

        string ConcatPath(params string[] paths);

        string MapPath(string relativePath);
    }
}