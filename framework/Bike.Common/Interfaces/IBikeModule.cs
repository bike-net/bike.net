﻿namespace Bike.Common.Interfaces
{
    public interface IBikeModule
    {
        /// <summary>Инициализация модуля. Выполняется когда все зависимости зарегистрированы</summary>
        void Initializing();

        /// <summary>Регистрация зависимостей. Выполняется перед инициализацией.</summary>
        void RegisterDependency();
    }
}