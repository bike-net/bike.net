﻿namespace Bike.Contracts.RedisClient
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>Интерфейс простого клиента для Redis</summary>
    public interface IRedisRepository<TEntity>
    {
        /// <summary>Удалить сущность</summary>
        /// <param name="entity">Сущность</param>
        void Delete(TEntity entity);

        /// <summary>Удалить все сущности одного типа</summary>
        void DeleteAll();

        /// <summary>Получить сущность по идентификатору</summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Сущность</returns>
        TEntity Get(string id);

        /// <summary>Получение всех сущностей</summary>
        /// <returns>Колекция сущностей</returns>
        IQueryable<TEntity> GetAll();

        /// <summary>Сохранить сущность</summary>
        /// <param name="entity">Сущность</param>
        void Save(TEntity entity);

        /// <summary>'Сохранить коллекцию сущностей</summary>
        /// <param name="entities">Коллекция сущностей</param>
        void SaveAll(IEnumerable<TEntity> entities);
    }
}