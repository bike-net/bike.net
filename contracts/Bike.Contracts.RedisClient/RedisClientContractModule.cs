﻿namespace Bike.Contracts.RedisClient
{
    using Bike.Common.Attributes;
    using Bike.Common.Interfaces;

    [BikeModule(RedisClientContractName, "0.0.1")]
    public class RedisClientContractModule : IBikeContractModule
    {
        private const string RedisClientContractName = "Contracts:RedisClient";

        /// <summary>Инициализация модуля. Выполняется когда все зависимости зарегистрированы</summary>
        public void Initializing()
        {
        }

        /// <summary>Регистрация зависимостей. Выполняется перед инициализацией.</summary>
        public void RegisterDependency()
        {
        }
    }
}