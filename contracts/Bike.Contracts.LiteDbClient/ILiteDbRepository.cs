﻿namespace Bike.Contaracts.LiteDbClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    public interface ILiteDbRepository<TEntity> : IDisposable
    {
        /// <summary>Добавление сущности</summary>
        /// <param name="entity">Сущность</param>
        /// <returns>Идентификатор новой сущности</returns>
        long Create(TEntity entity);

        /// <summary>Добавление сущностей</summary>
        /// <param name="entities">Сущности</param>
        /// <returns>Количество вставленных сущностей</returns>
        long Create(IEnumerable<TEntity> entities);

        /// <summary>Удаление сущностей</summary>
        /// <param name="id">Идентификатор сущности</param>
        /// <returns>Успешность удаления</returns>
        bool Delete(long id);

        /// <summary>Получение сущности по идентификатору</summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Сущность</returns>
        TEntity Get(long id);

        /// <summary>Получение сущностей по условию</summary>
        /// <param name="filter">Условие отбора</param>
        /// <param name="skip">Колиество пропущеных сущностей</param>
        /// <param name="limit">Количество взятых сущностей</param>
        /// <returns>Отобранные по фильтру сущности</returns>
        IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> filter, int skip = 0, int limit = int.MaxValue);

        /// <summary>Обновление сущности</summary>
        /// <param name="entity">Сущность</param>
        /// <returns>Успешность обновления</returns>
        bool Update(TEntity entity);
    }
}