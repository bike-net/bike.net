﻿namespace Bike.Contaracts.LiteDbClient
{
    using Bike.Common.Attributes;
    using Bike.Common.Interfaces;

    [BikeModule(LiteDbClientName, "0.0.1")]
    public class BikeLiteDbClientModule : IBikeContractModule
    {
        private const string LiteDbClientName = "Contracts:LiteDbClient";

        public void Initializing()
        {
        }

        public void RegisterDependency()
        {
        }
    }
}