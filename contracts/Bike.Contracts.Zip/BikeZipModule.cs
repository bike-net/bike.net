﻿namespace Bike.Contracts.Zip
{
    using Bike.Common.Attributes;
    using Bike.Common.Interfaces;

    [BikeModule(ZipName, "0.0.1")]
    public class BikeZipModule : IBikeContractModule
    {
        private const string ZipName = "Contracts:Zip";

        public void Initializing()
        {
        }

        public void RegisterDependency()
        {
        }
    }
}