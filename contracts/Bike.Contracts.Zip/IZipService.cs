﻿namespace Bike.Contracts.Zip
{
    using System.IO;

    public interface IZipService
    {
        Stream ZipFiles(params string[] relativePaths);
    }
}