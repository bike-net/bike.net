﻿namespace Bike.Contracts.FileSystem
{
    using Bike.Common.Attributes;
    using Bike.Common.Interfaces;

    [BikeModule(FileSystemName, "0.0.1")]
    public class BikeFileSystemModule : IBikeContractModule
    {
        private const string FileSystemName = "Contracts:FileSystem";

        public void Initializing()
        {
        }

        public void RegisterDependency()
        {
        }
    }
}