﻿namespace Bike.Contracts.FileSystem
{
    using System.IO;

    public interface IFileService
    {
        Stream CreateTempFile(string relativeFolderPath);

        bool IsExist(string relativePath);

        Stream OpenFile(string relativePath, FileMode fileMode, FileAccess fileAccess, FileShare fileShare);
    }
}