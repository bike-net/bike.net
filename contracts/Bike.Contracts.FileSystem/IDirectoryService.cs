﻿namespace Bike.Contracts.FileSystem
{
    using System;

    public interface IDirectoryService
    {
        void Create(string relativePath);

        IDisposable CreateTempDirectory(out string relativePath);

        void Delete(string relativePath);

        bool IsExist(string relativePath);
    }
}