﻿namespace Bike.Contracts.Web
{
    public interface IBundleRegistration
    {
        void AddCss(string moduleName, string relativePath);

        void AddCssDirectory(string moduleName, string relativePath, string searchPattern = "*.css");

        void AddScript(string moduleName, string relativePath);

        void AddScriptDirectory(string moduleName, string relativePath, string searchPattern = "*.js");

        void Register();
    }
}