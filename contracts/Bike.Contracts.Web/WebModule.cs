﻿namespace Bike.Contracts.Web
{
    using Bike.Common.Attributes;
    using Bike.Common.Interfaces;

    [BikeModule(WebModuleName, "0.0.1")]
    public class WebModule : IBikeContractModule
    {
        private const string WebModuleName = "Contracts:Web";

        public void Initializing()
        {
        }

        public void RegisterDependency()
        {
        }
    }
}